# INSTALLATION
1. Clone this repository using command `git clone git@bitbucket.org:natanelia/bni-scraper.git`
2. Run `npm install`
3. Run `npm run prod`, or `npm start` (using node-dev)
4. Open web browser and navigate to [localhost:5555](http://localhost:5555)

This app has been created with Node v0.10.32.
Please kindly report if it doesn't work as expected.

# RESULT
```
#!json

{
 "Fashion": [
  {
   "title": "GRATIS Gelang Swarovski dengan Kartu Debit BNI ",
   "logoImage": "http://m.bnizona.com/files/6efcf507ce5bd867ad106939fc2110ae.jpg",
   "merchantName": "Shafira",
   "validUntil": "03 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1169",
   "content": "<strong>Syarat &amp; Ketentuan : </strong><br><br>\n- Transaksi minimal Rp 450.000,- dengan Kartu Debit BNI, mendapatkan 1 point dan berlaku kelipatan.<br><br>\n- Setiap 8 point ditukarkan dengan 1 unit gelang Swaroski.<br><br>\n- Untuk transaksi minimal Rp3,6 juta, dapat langsung memperoleh 1 unit gelang Swaroski.<br><br>\n- Nilai minimum transaksi, setelah dikurangi diskon promo reguler 20% untuk Kartu Debit BNI<br><br>\n- Berlaku di seluruh outlet Shafira untuk pembelian all produk Shafira<br><br>\n- Berlaku untuk seluruh Kartu Debit BNI kecuali Kartu Debit BNI Syariah.<br><br>\n- Periode program hingga 03 April 2016.",
   "image": "http://m.bnizona.com//files/edee1619b77ff8e6af055dbf40bafa8c.jpg",
   "location": [
    "http://www.shafira.com/"
   ]
  },
  {
   "title": "BNI - ZALORA THURSDAY SPECIAL",
   "logoImage": "http://m.bnizona.com/files/83481f460ae09ecbee0096962d38d6ce.png",
   "merchantName": "www.zalora.co.id",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1134",
   "content": "Dapatkan penawaran spesial Kartu Kredit BNI untuk berbagai produk fashion di <a href=\"http://bni.us2.list-manage.com/track/click?u=f81fd17ae655f9dcf0356d9f4&amp;id=5f174fa33f&amp;e=360cdb0a9c\" target=\"_blank\">Zalora.co.id</a>, masukkan kode voucher <strong>BNIKAMIS</strong> dan nikmati potongan 30% untuk transaksi belanja Anda.<br><br>\n<strong>Mekanisme :</strong><br><br>\n- Diskon 30% berlaku untuk minimum pembelanjaan Rp 350.000,-.<br><br>\n- Promo berlaku setiap hari Kamis hingga 31 Maret 2016.<br><br>\n- Promo tidak berlaku untuk produk dan <em>brand non-sale items</em>. Info selanjutnya di <a href=\"http://bni.us2.list-manage.com/track/click?u=f81fd17ae655f9dcf0356d9f4&amp;id=1f373ba8e0&amp;e=360cdb0a9c\" target=\"_blank\">zalora.co.id/faq-non-sale</a><br><br>\n- Gunakan kode voucher : <strong>BNIKAMIS </strong>pada saat pembayaran.<br><br>\n- Kode voucher dapat digunakan lebih dari 1x selama program berlangsung,<br><br>\n- Kode voucher tidak dapat digabungkan dengan promo lainnya.<br><br>\n- Berlaku untuk pembayaran dengan semua Kartu Kredit BNI, kecuali iB Hasanah dan Corporate Card.<br><br>\nInformasi lebih lengkap hubungi BNI Call di 1500046 atau 68888 melalu ponsel.",
   "image": "http://m.bnizona.com//files/2234d355a1d5bdf2df022169b5cfdd7b.jpg",
   "phone": "02129490100",
   "location": [
    "www.zalora.co.id"
   ]
  },
  {
   "title": "Cicilan 0% untuk 3 & 6 bulan di Euroskinlab",
   "logoImage": "http://m.bnizona.com/files/2b6cf24b89f4d0cb20fa6c64470e3f22.jpg",
   "merchantName": "EuroSkinLab",
   "validUntil": "31 October 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1124",
   "content": "- Cicilan 0% untuk 3 &amp; 6 bulan dengan minimal transaksi Rp1,5 juta &#xA0;<br><br>\n- Promo berlaku untuk semua Kartu Kredit BNI (kecuali Hasanah &amp; Corporate Card)&#xA0;&#xA0; &#xA0;&#xA0;&#xA0; &#xA0;<br><br>\n- Promo diskon dan cicilan dapat digabungkan&#xA0;&#xA0; &#xA0;&#xA0;&#xA0; &#xA0;<br><br>\n- Berlaku hingga 31 Oktober 2016",
   "image": "http://m.bnizona.com//files/4ae5658c876e5a7bc4974c6cbe2ce2e8.jpg",
   "phone": "0217226466",
   "location": [
    "JAKARTA SELATAN Jl. Iskandarsyah Raya No. 97, Kebayoran Baru, 0217226466",
    "JAKARTA UTARA Komplek Gading Bukit Indah Blok A 1011,Kelapa Gading, 0214513624",
    "JAKARTA UTARA Komplek Galeri Niaga Mediterania II, Blok K 8N, Pantai Indah Utara II, PIK, 0215880492",
    "JAKARTA BARAT Komplek Sentra Niaga Blok T1/20, Puri Indah, 02158301458",
    "SURABAYA Bukit Darmo Boulevard 10 CC, 0317346243",
    "MEDAN Jl Perintis Kemerdekaan, Komplek Jati Junction Blok K21, 06180501258"
   ]
  },
  {
   "title": "Cicilan 0% di Bimbi.com dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/4d59952517d21daf4a16fdbeec17379b.jpg",
   "merchantName": "Bimbi.com",
   "validUntil": "30 June 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1120",
   "content": "Periode program: hingga 30 Juni 2016<br><br>\n<strong>Syarat &amp; Ketentuan:</strong><br>\n<ul><br>\n\t<li>Cicilan 0% untuk periode cicilan 3 dan 6 bulan di <a href=\"http://www.bimbi.com\" target=\"_blank\">www.bimbi.com</a>.</li><br>\n\t<li>Minimum transaksi yang dapat dicicil Rp2.000.000,- untuk tenor 3 bulan atau Rp5.000.000,- untuk tenor 6 bulan.</li><br>\n\t<li><em>Installment </em>langsung dilakukan di website <a href=\"http://www.bimbi.com\" target=\"_blank\">www.bimbi.com</a>.</li><br>\n\t<li>Berlaku untuk semua jenis Kartu Kredit BNI kecuali iB Hasanah dan Corporate Card.</li><br>\n</ul><br>\nInformasi lebih lengkap hubungi BNI Call di <strong>1500046</strong><br><br>\n&#xA0;",
   "image": "http://m.bnizona.com//files/4d348fc12436aa42d92d070046c149d0.jpg",
   "phone": "(+62)2129305555 ",
   "location": [
    "http://www.bimbi.com/"
   ]
  },
  {
   "title": "Diskon 10% di EVB dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/5325a2ce1fd2a9d3bbee1a5a76ead381.jpg",
   "merchantName": "EVB",
   "validUntil": "30 September 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1115",
   "content": "- Diskon 10% dengan Kartu Kredit BNI kecuali iB Hasanah dan Corporate Card.<br><br>\n- Promo hingga 30 September 2016.<br><br>\n&#xA0;",
   "image": "http://m.bnizona.com//files/23886e44e93ef2a44ac759f95b520136.jpg",
   "phone": "02145855056",
   "location": [
    "EVB Kelapa Gading",
    "EVB Gandaria City",
    "EVB Galaxy Surabaya",
    "EVB Manado Town Square",
    "EVB Paris Van Java",
    "EVB Centre Point Medan",
    "EVB Mal SKA",
    "EVB Mall Ratu Indah Makassar",
    "EVB Panakkukang Makassar",
    "EVB Trans Studio",
    "EVB Balikpapan",
    "EVB Oberoi Seminyak"
   ]
  },
  {
   "title": "Hemat hingga 100% dengan BNI Reward Points di Superdry",
   "logoImage": "http://m.bnizona.com/files/9c161fd3e5e6afba21dd3c41e29e4e3f.jpg",
   "merchantName": "Superdry",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1114",
   "content": "- Diskon 5% untuk shopping fashion terkini di Superdry dengan Kartu Kredit BNI hingga 31 Januari 2016<br><br>\n- Hemat hingga 100% dengan BNI Reward Points hingga 31 Maret 2016<br><br>\n- Cicilan 0% 3 bulan (mininimal transaksi Rp1,5 jt), 6 bulan (mininimal transaksi Rp2,5 jt) &amp; 12 bulan (mininimal transaksi Rp5,5 jt)<br><br>\n- Promo berlaku untuk semua Kartu Kredit BNI (kec iB Hasanah Card &amp; Corporate card)<br><br>\n- Berlaku untuk di seluruh Store Superdry",
   "image": "http://m.bnizona.com//files/c4c4a658e522aa6437b481e1abd47534.jpg",
   "location": [
    "Kelapa Gading Mall",
    "Summarecon Mal Serpong",
    "Karawaci Supermall",
    "Citraland",
    "Emporium Pluit Mall",
    "Tunjungan Plaza"
   ]
  },
  {
   "title": "Diskon 15% di Point Break dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/ee6795b4b96f323e930c9d88139f8c03.jpg",
   "merchantName": "Point Break",
   "validUntil": "30 June 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1112",
   "content": "Dapatkan diskon 15% Regular Items Point Break untuk pemegang Kartu Kredit BNI kecuali Corporate Card dan iB Hasanah Card. Periode promo berakhir pada 30 Juni 2016 dan berlaku di seluruh outlet Point Break.<br><br>\nInfo lengkap hubungi BNI Call 1500046.",
   "image": "http://m.bnizona.com//files/a204544e6a202129800ba45d139e295f.jpg",
   "location": [
    "http://www.jlindonesia.com/id"
   ]
  },
  {
   "title": "Cantik dengan THEFACESHOP diskon hingga 15%",
   "logoImage": "http://m.bnizona.com/files/9b5895a577917f999c70aecc6a15088c.jpg",
   "merchantName": "THEFACESHOP",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1109",
   "content": "- Diskon 15% regular items untuk pemegang Kartu Kredit BNI + VIP Member THEFACESHOP<br><br>\n- Diskon 10% regular items untuk pemegang Kartu Kredit BNI (non member)<br><br>\n- Berlaku untuk transaksi dengan Kartu Kredit BNI kecuali Corporate Card &amp; iB Hasanah Card<br><br>\n- Berlaku hingga 31 Maret 2016",
   "image": "http://m.bnizona.com//files/5d8c3069ee73ea2c4466b4fc7577b3ef.jpg",
   "phone": "021 45845932",
   "location": [
    "Surabaya Town Square",
    "Galeria Bali",
    "Summarecon Mall Serpong",
    "Plaza Senayan",
    "Tunjungan Plaza 4 Surabaya",
    "Senayan City Jakarta",
    "Pondok Indah Mall Jakarta",
    "Gandaria City Jakarta",
    "Citraland Jakarta",
    "Botani Square Bogor",
    "Paragon Mall Semarang",
    "Mega Mall Bekasi",
    "Manado Town Square",
    "Mall Artha Gading",
    "Pakuwon Trade Center Surabaya",
    "Lippo Karawaci Tangerang",
    "Cibubur Junction",
    "Mall Taman Anggrek",
    "Living World Alam Sutera",
    "Grand Indonesia",
    "Cilandak Town Square",
    "Central Park Jakarta",
    "Emporium Mall Pluit"
   ]
  },
  {
   "title": "Shopping brand Quiksilver dan DC Shoes diskon 10% ",
   "logoImage": "http://m.bnizona.com/files/542da19361af63b1eb52fca7f17512cc.jpg",
   "merchantName": "Quiksilver",
   "validUntil": "18 March 2017",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1108",
   "content": "Tampil gaya dengan brand Quiksilver dan DC Shoes.<br><br>\n- Diskon 10% untuk pembelian 2 pcs dengan Kartu Kredit BNI kecuali Corporate Card &amp; iB Hasanah Card<br><br>\n- Promo hingga 18 Maret 2017.<br><br>\n- Berlaku di seluruh outlet Quiksilver &amp; DC Shoes.<br><br>\nInfo lengkap hubungi BNI Call <strong>1500046</strong>",
   "image": "http://m.bnizona.com//files/3ccc85b69b543b0e8b9a2dd14a34be12.png",
   "location": [
    "QUIKSILVER PLAZA AMBARUKMO",
    "JL.Laksda Adisucipto, Yogyakarta 55281, Indonesia",
    "QUIKSILVER GRAND METROPOLITAN",
    "Grand Metropolitan Mall Bekasi Lt. UG",
    "Jl. K. H. Noer Alie, Kav 8 &#x2013; Pekayon &#x2013; Bekasi Barat",
    "QUIKSILVER TRANS STUDIO MAKASSAR",
    "Trans Studio Mall Lt. 1 Unit 29, Kawasan Terpadu Trans Studio",
    "Jl. HM DG Patompo Metro Tanjung Bunga",
    "Makassar  Sulawesi Selatan  90134",
    "DC SHOES STORE",
    "TRANS STUDIO MALL BANDUNG",
    "JL GATOT SUBROTO NO 289 UNIT 178/180 LT 1",
    "BANDUNG, 40273",
    "DC Store Malioboro Mall",
    "Jalan Malioboro No. 5258 Unit 08 Yogyakarta"
   ]
  },
  {
   "title": "Diskon 10% untuk brand Hurley dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/07f158d119c0dfd5b77906c87fd7fedf.jpg",
   "merchantName": "Hurley",
   "validUntil": "18 March 2017",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1106",
   "content": "Maksimalkan gaya sport dengan brand Hurley dan dapatkan diskon 10% untuk pembelian minimal 2 pcs dengan Kartu Kredit BNI. Ketentuan:<br>\n<ul><br>\n\t<li>Berlaku untuk semua Kartu Kredit BNI</li><br>\n\t<li>Berlaku di outlet Grand Indonesia, Pondok Indah Mall, Plaza Ambarukmo dan Centre Point Medan</li><br>\n\t<li>Promo berakhir pada 18 Maret 2017</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/ade0e6ed22d208d35d9e66cdbf011493.jpg",
   "phone": "02123581033",
   "location": [
    "HURLEY GRAND INDONESIA",
    "Grand Indonesia Mall Shopping Town",
    "Lt2 SkyBridge No.19  Jakarta",
    "HURLEY CENTRE POINT",
    "JL. TIMOR Mall Centre Point Lt UG 07 MEDAN",
    "HURLEY PONDOK INDAH MALL",
    "JL. Metro Pondok Indah Lt.3 Unit.307 PIM2  Jakarta",
    "HURLEY AMBARUKMO PLAZA",
    "Jl.Laksda Adisucipto Yogyakarta"
   ]
  },
  {
   "title": "Diskon 20% Untuk Reguler Items Giordano, Giordano Junior dan Giordano Ladies dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/5a4372643447cc557de063955fc5c150.jpg",
   "merchantName": "Giordano",
   "validUntil": "18 March 2017",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1105",
   "content": "Gaya sekeluarga dengan Giordano, Giordano Junior dan Giordano Ladies. Diskon 20% untuk transaksi dengan Kartu Kredit BNI (kecuali Corporate Card dan iB Hasanah Card). Promo berakhir pada 18 Maret 2017 dan berlaku di seluruh outlet Giordano.",
   "image": "http://m.bnizona.com//files/9d422e93ec31b3f9affeb65e174cb7df.jpg",
   "location": [
    "Seluruh outlet"
   ]
  },
  {
   "title": "Shop & Trip @Centro & @Parkson Departement Store     ",
   "logoImage": "http://m.bnizona.com/files/cfabf057560e5cbfd6fc62e988864c55.jpg",
   "merchantName": "BNI Experience",
   "validUntil": "29 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/1085",
   "content": "Periode : hingga 29 Februari 2016<br><br>\nSyarat &amp; Ketentuan:<br><br>\na. Minimum transaksi Rp 500.000 dengan Kartu Debit BNI mendapatkan 1 kupon undian dan berlaku kelipatan.<br><br>\nb. Khusus nasabah yang memiliki kartu Parkson Centro Card akan mendapatkan 1 kupon tambahan (tidak berlaku kelipatan)<br><br>\nc. 4 Hadiah utama berupa voucher paket wisata 3D2N di Resort World&#x2122; Sentosa Singapura untuk masing-masing 2 orang setiap pemenang.<br><br>\nd. Paket meliputi Menginap 3 hari 2 malam dengan pilihan Hard Rock Hotel, Hotel Michael, dan Festive Hotel, paket wisata termasuk:<br><br>\n- Sarapan untuk 2 orang<br><br>\n- Tiket Universal Studios Singapore untuk 2 orang<br><br>\n- Tiket S.E.A. Aquarium Tickets untuk 2 orang<br><br>\n- Tiket Adventure Cove Waterpark Tickets untuk dua orang<br><br>\ne. Hadiah lainnya berupa voucher belanja @500 rb untuk 50 orang pemenang<br><br>\nf. Pajak hadiah menjadi tanggung jawab BNI.<br><br>\ng. Promo hanya berlaku bagi Kartu Debit BNI MasterCard.(tidak berlaku untuk kartu private label)<br><br>\nh. Berlaku di seluruh outlet Centro dan Parkson di seluruh Indonesia.",
   "image": "http://m.bnizona.com//files/5c6a82093048eac55e5c9c7d62f5d5b2.jpg",
   "location": [
    "bniexperience@outlook.com"
   ]
  },
  {
   "title": "Installment 0% di Mardhatillah",
   "logoImage": "http://m.bnizona.com/files/0ac26bdc00e7e2a4ed51ffefc13b932e.jpg",
   "merchantName": "Mardhatillah",
   "validUntil": "01 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/826",
   "content": "<ul><br>\n\t<li>Installment 0% periode 3, 6 dan 12 bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis kartu kredit BNI (kecuali Hasanah Card dan Corporate Card)</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/88c2f83cb39140d39d395821c2f7f26a.jpg",
   "location": [
    "Pasar Bawah Blok L No.01 &amp; 6 Lt. 2 Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di D Baby Corner",
   "logoImage": "http://m.bnizona.com/files/197389ee9cbb60977fd093942e057d3a.jpg",
   "merchantName": "D Baby Corner",
   "validUntil": "30 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/790",
   "content": "<ul><br>\n\t<li>Installment 0% periode 3, 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/8bc1cde60de935e165c25b43cf3ca09a.jpg",
   "location": [
    "Jl. Kakap No.23 Tangkerang Selatan Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di Susan Baby Shop",
   "logoImage": "http://m.bnizona.com/files/e49f0e33c14a8e1869633614ac2cec6a.jpg",
   "merchantName": "Susan Baby Shop",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/788",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/743f90bf421034ec5d4aa2c0922f4032.jpg",
   "location": [
    "Jl. Setia Budhi No.65 A Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di Nathan's",
   "logoImage": "http://m.bnizona.com/files/fd39145055c5068aa828af0f0ddd693c.jpg",
   "merchantName": "Nathan's",
   "validUntil": "22 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/735",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/66262ffe25faafac47c002c5730cd742.jpg",
   "location": [
    "Jl. Proklamasi No.11 Padang"
   ]
  },
  {
   "title": "Installment 0% di Verona",
   "logoImage": "http://m.bnizona.com/files/2450f63b7ba857dd59103b6633db2d7a.jpg",
   "merchantName": "Verona",
   "validUntil": "26 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/16/697",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12 bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/9ede84176985ebdab0c7ea98ecf3fdea.jpg",
   "location": [
    "Jl. Jend Sudirman No.122 ABC Pekanbaru"
   ]
  }
 ],
 "Groceries": [
  {
   "title": "Gratis 5 Produk di Tip Top Swalayan dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/9ce11ea608637f8f53b4d19220ccb827.jpg",
   "merchantName": "Tip Top Swalayan",
   "validUntil": "03 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/17/1163",
   "content": "- Berlaku untuk semua jenis Kartu Kredit BNI kecuali Corporate Card &amp; iB Hasana.<br><br>\n- Periode program : hingga 3 April 2016<br><br>\n<strong>Syarat &amp; Ketentuan :</strong><br><br>\n- Gratis 5 Produk akan diberikan dengan syarat minimum transaksi Rp500 ribu/ struk/ kartu/ hari.<br><br>\n- Gratis 5 Produk : Minyak Sania 1Lt, Gulaku 500gr, Forvita 200gr, Bango Kecap Manis 220ml, Indomie Goreng 5 bungkus.<br><br>\n- Maksimum pemberian Gratis 5 produk adalah 1 kali/ kartu/ hari.<br><br>\n- Jenis syarat transaksi hanya berlaku untuk produk <em>groceries.</em><br><br>\n- Tidak berlaku penggabungan promo.<br><br>\n- Syarat dan ketentuan lain berlaku.",
   "image": "http://m.bnizona.com//files/259d56d3dd0cce7d8534c64bea19e05e.jpg",
   "phone": "0214892154",
   "location": [
    "Jl. Balai Pustaka Timur 3135",
    "Rawamangun, Jakarta Timur"
   ]
  },
  {
   "title": "Tambahan diskon 20% fresh product di LOTTEMart Hypermaket dan Supermaket",
   "logoImage": "http://m.bnizona.com/files/63ba74328ee763a5c68f8e7c98a6c3a9.jpg",
   "merchantName": "LOTTEMart Hypermarket",
   "validUntil": "30 June 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/17/1121",
   "content": "<strong>Syarat &amp; Ketentuan:</strong><br><br>\n1. Berlaku untuk seluruh jenis Kartu Kredit BNI kecuali Corporate Card dan iB Hasanah<br><br>\n2. Tambahan diskon 20% untuk produk segar tertentu setiap hari Senin s.d. Kamis selama periode 11 Januari &#x2013; 30 Juni 2016, tanpa adanya minimum transaksi.<br><br>\n3. Jenis produk segar sebagai berikut:<br><br>\n- Hari Senin : kategori daging dan ikan<br><br>\n- Hari Selasa : kategori Bakery &amp; RTE (<em>ready to eat</em>)<br><br>\n- Hari Rabu : kategori buah<br><br>\n- Hari Kamis : kategori sayuran<br><br>\n4. Berlaku untuk 1 transaksi/kartu/hari<br><br>\n5. Maksimum diskon Rp100.000,- /transaksi/kartu/hari<br><br>\n6. Tidak dapat digabungkan dengan promo lain.",
   "image": "http://m.bnizona.com//files/6e1eb6221ac6a598bd19f2722c0b1213.jpg",
   "location": [
    "Festival City Link (ex Mollis) Bandung | Panakukang, Makasar | Centre Point, Medan | The Park Mall, Solo | Gandaria City, Jakarta | Ratu Plaza, Jakarta | Kelapa Gading, Jakarta | Kuningan City, Jakarta | Fatmawati, Jakarta | Bekasi Junction, Bekasi | Taman Surya, Jakarta | Bintaro, Tangerang Selatan | Cimone, Tangerang"
   ]
  }
 ],
 "Food and Beverage": [
  {
   "title": "BNI Dining Experience",
   "logoImage": "http://m.bnizona.com/files/cfabf057560e5cbfd6fc62e988864c55.jpg",
   "merchantName": "BNI Experience",
   "validUntil": "31 January 2017",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1153",
   "content": "Nikmati sajian istimewa menyambut Imlek bersama keluarga dan orang terkasih di resto favorit Anda. Gunakan selalu Kartu Kredit dari Bank Negara Indonesia untuk mendapatkan berbagai penawaran menarik.<br><br>\n<img alt=\"\" src=\"/files/images/2cf2dd2c852b5b85e2d2387a1543bedb.png\" style=\"height:666px; width:400px\"><br><br>\n* BRP = BNI Reward Point<br><br>\n* Syarat &amp; Ketentuan berlaku",
   "image": "http://m.bnizona.com//files/265110ffe470d699a5553a1c46d0bb88.jpg",
   "location": [
    "bniexperience@outlook.com"
   ]
  },
  {
   "title": "Nikmati Seafood lezat di The Manhattan Fish Market dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/e3a939d49777cf2327d11f70058a87a6.png",
   "merchantName": "The Manhattan Fish Market",
   "validUntil": "28 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1152",
   "content": "Bersama orang terkasih, nikmati seafood lezat di The Manhattan Fish Market dengan Kartu Kredit BNI.<br><br>\nKetentuan:<br><br>\n- Diskon 20% untuk minimum transaksi Rp 200.000,- dan maksimum transaksi Rp 2.000.000,-<br><br>\n- Diskon berlaku untuk makanan saja (food only)<br><br>\n- Promo berlaku untuk Kartu Kredit BNI Gold, BNI Titanium, Platinum, Infinite termasuk Co Branding dan Affinity kecuali iB Hasanah Card dan Corporate Card<br><br>\n<br><br>\n&#xA0;",
   "image": "http://m.bnizona.com//files/337da797b3faa423b8cae1c583c83d7c.jpg",
   "phone": "021 2916 8368",
   "location": [
    "AEON Mall BSD",
    "level G #25 Jl. BSD Raya Utama",
    "Tel: 021 2916 8368",
    "Bali",
    "South Kuta Beach, Jl. Kartika Plaza 10,",
    "Denpasar, Bali",
    "Central Park Mall",
    "Lv LG #204205  Jl. Let. Jend. S. Parman no. 28 Kav. 34",
    "Jakarta Barat",
    "Grand Indonesia Shopping Town",
    "Sky Bridge Lv 3A #0606A  Jl. M.H. Thamrin no. 1",
    "Jakarta Pusat",
    "Lippo Mall Kemang",
    "Lv 3 #02 Jakarta Selatan",
    "Lotte Shopping Avenue",
    "Lv 4 #36 Jakarta Selatan",
    "Medan",
    "Jl. Timor, Ruko Center Point Blok F1 Medan"
   ]
  },
  {
   "title": "Diskon hingga 33% santap di Grand Suki dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/9ca3491e3432942cf0fda60e13b1535f.png",
   "merchantName": "Grand Suki",
   "validUntil": "30 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1151",
   "content": "Nikmati diskon hingga 33% untuk santap istimewa di Grand Suki dengan Kartu Kredit BNI.<br><br>\nKetentuan:<br><br>\n- Diskon sebesar 33% food only di Weekdays (Senin - Jumat).&#xA0; &#xA0;<br><br>\n- Diskon sebesar 25%&#xA0; food only di Weekend (Sabtu &amp; Minggu)<br><br>\n- Min. transaksi Rp. 350.000,- dan max. transaksi Rp. 1.500.000,- (sebelum diskon, pajak, dan service)&#xA0; &#xA0;<br><br>\n- Promo berlaku untuk <em>dine in </em>(tidak berlaku take away)&#xA0; &#xA0;<br><br>\n- Promo tidak dapat digabungkan dengan promo lainnya yang sedang berlangsung&#xA0; &#xA0;<br><br>\n- Berlaku untuk seluruh Kartu Kredit BNI kecuali Corporate Card dan iB Hasanah Card",
   "image": "http://m.bnizona.com//files/d21706bbc21759046adf43938a3479c0.jpg",
   "phone": "(021) 2358 1096",
   "location": [
    "Grand Indonesia Sky Bridge Level 5 Shop",
    "Unit FD 2  01, Jakarta"
   ]
  },
  {
   "title": "Diskon 20% di Imperial Chef dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/eec79aa555e7cd5c4ffaabb0451700f7.png",
   "merchantName": "Imperial Chef",
   "validUntil": "30 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1149",
   "content": "Makan istimewa bersama keluarga di Imperial Chef dengan Kartu Kredit BNI.<br><br>\nKetentuan:<br><br>\n- Diskon 20% (food &amp; beverage) dengan minimum transaksi Rp. 500.000,-<br><br>\n- Maksimum transaksi Rp. 1.500.000,- (before tax &amp; service)<br><br>\n- Semua Kartu Kredit BNI Titanium, Platinum dan Infinite dan tidak berlaku untuk jenis BNI Classic/Silver Card, Gold Card, Corporate Card, &amp; iB Hasanah Card<br><br>\n- Promo diskon tidak berlaku untuk set menu dan minuman beralkohol<br><br>\n- Tidak berlaku split transaksi dan tidak dapat digabungkan dengan promo lainnya<br><br>\n- Promo hanya berlaku untuk <em>dine in</em>",
   "image": "http://m.bnizona.com//files/9a7f1efc69639428155602f58fe4a327.jpg",
   "phone": "021  6262533",
   "location": [
    "Hotel Jayakarta",
    "The Food Place",
    "Jl. Hayam Wuruk No. 126, Jakarta 11180",
    "Telp : 021  6262533/ 534",
    "Lotte Shopping Avenue",
    "Jl. Prof. Dr. Satrio Kav 3  5 Lt. 3F, Karet Kuningan, Jakarta Selatan",
    "Telp: 021  29889381/ 82",
    "Galaxy Mall",
    "Imperial Chef  Galaxy Mall,",
    "Lt.2 no.248, Jl. Dharmahusada Indah Timur no.32",
    "Phone : 031  5937 194/ 5",
    "Fax : 031  5937 322"
   ]
  },
  {
   "title": "Diskon hingga 33% santap di Han Gang Korean Restaurant dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/1df466566de3fdebb53e86addecc1427.jpg",
   "merchantName": "Han Gang",
   "validUntil": "30 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1148",
   "content": "Nikmati diskon hingga 33% untuk santap istimewa di Han Gang Korean Restaurant dengan Kartu Kredit BNI.<br><br>\nKetentuan:<br><br>\n- Diskon sebesar 33% food only di Weekdays (Senin - Jumat).&#xA0; &#xA0;<br><br>\n- Diskon sebesar 25%&#xA0; food only di Weekend (Sabtu &amp; Minggu)<br><br>\n- Min. transaksi Rp. 350.000,- dan max. transaksi Rp. 1.500.000,- (sebelum diskon, pajak, dan service)&#xA0; &#xA0;<br><br>\n- Promo berlaku untuk <em>dine in </em>(tidak berlaku take away)&#xA0; &#xA0;<br><br>\n- Promo tidak dapat digabungkan dengan promo lainnya yang sedang berlangsung&#xA0; &#xA0;<br><br>\n- Berlaku untuk seluruh Kartu Kredit BNI kecuali Corporate Card dan iB Hasanah Card",
   "image": "http://m.bnizona.com//files/a6482c428aeae99ed0997dfa9aa2db51.jpg",
   "phone": "+62 21 23580710",
   "location": [
    "Senayan City LG Floor Unit L 09 A , Jakarta (021) 7278 1231",
    "Pacific Place Lt 5 unit 37, Jakarta (021) 5797 3460",
    "Mall Taman Anggrek Lt 4, Jakarta (021) 5639 499",
    "Grand Indonesia Sky Bridge Level 3 A Shop Unit FD 1  08, Jakarta (021) 2358 0710",
    "Jl. Wolter Monginsidi No 99 , Jakarta (021) 7278 7802",
    "Living World Alam Sutera GF, Unit G  37 &amp; 37 A",
    "Serpong (021) 5312 5496",
    "Emporium Pluit Mall GF Unit G 3536, Jakarta (021) 290 71925",
    "Plaza Senayan, Food Court Lt.3, Shop Unit ID 329 C (021) 572 4802",
    "Pondok Indah Mall, Street Gallery Lt 1 No 107108, Jakarta",
    "Baywalk Pluit"
   ]
  },
  {
   "title": "Diskon 15% dengan Kartu Kredit BNI di Shokupan",
   "logoImage": "http://m.bnizona.com/files/8db79f0d9134dd99ee9bdcc284cada6d.png",
   "merchantName": "Shokupan Bakery & Cakes",
   "validUntil": "15 May 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1147",
   "content": "Transaksi Bakery &amp; Cake di Shokupan bisa dapat diskon 15% dengan Kartu Kredit BNI.<br><br>\nKetentuan:<br><br>\n- Dapatkan Diskon 15% minimum transaksi Rp 150.000,- maksimum transaksi Rp 1.500.000,-<br><br>\n- Promo berlaku untuk seluruh produk (kecuali item promo, item paket dan produk hampers)<br><br>\n- Promo berlaku untuk semua Kartu Kredit BNI kecuali Corporate &amp; iB Hasanah Card<br><br>\n- Promo berlaku setiap hari termasuk hari libur Nasional<br><br>\n- Tidak berlaku split bill<br><br>\n- Tidak dapat digabungkan dengan promo lainnya",
   "image": "http://m.bnizona.com//files/caa71e277ca197fa1b5c367cb934a5b1.jpg",
   "phone": "02169837452",
   "location": [
    "Plaza Indonesia",
    "Plaza BII",
    "Plaza Senayan, Food Hall",
    "Plaza UOB",
    "Senayan City, Food Hall",
    "Kuningan City",
    "Kota Kasablanka",
    "Cyber 2 Tower",
    "Menara Jamsostek",
    "Alamanda Tower",
    "Menara Karya",
    "Graha Suveyor Indonesia",
    "Setia Budi Food Hall",
    "Bellagio Mall"
   ]
  },
  {
   "title": "Free Payung di Rumah Buah dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/1803ad0fb4b2f201b0dad05500ae69c6.png",
   "merchantName": "Rumah Buah",
   "validUntil": "14 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1146",
   "content": "Musim hujan perlu payung. Belanja di Rumah Buah, Gratis Payung.<br><br>\nKetentuan:<br><br>\n- Gratis Payung untuk minimum transaksi Rp 350.000,-<br><br>\n- Promo berlaku untuk semua Kartu Kredit BNI kecuali Kartu Kredit BNI Corporate, Garuda BNI Platinum &amp; Siganture dan iB Hasanah Card<br><br>\n- Promo hanya berlaku di outlet Total Buah/Rumah Buah yang bekerjasama dengan BNI<br><br>\n- Tidak berlaku kelipatan dan split transaksi",
   "image": "http://m.bnizona.com//files/e24e97d5b380a52d7b3147bb4a6770f6.jpg",
   "phone": "02154209695",
   "location": [
    "Outlet Tomang",
    "Jl. Tomang Raya No. 52, Blok A, Kav. No. 7, Kelurahan Jatipulo, Kecamatan Palmerah, Jakarta Barat",
    "Outlet Sukajadi",
    "My Fair Building Jl. Sukajadi No. 228 Bandung Jawa Barat",
    "Outlet Tirtayasa",
    "Jl. Sutan Tirtayasa No. 22 Bandung Jawa Barat",
    "Outlet Gading Serpong",
    "Jl. Boulevard Gading Serpong Kav Rotunda No. 3 Summarecon Tangerang",
    "Outlet Cibubur",
    "Rest Area Cibubur Square Km 10.6 Tol Jakarta Bogor",
    "Outlet Karang Tengah",
    "Rest Area Palm Square Km 13.5 Tol Jakarta Merak",
    "Outlet Alam Sutera",
    "Kawasan Flavor Bliss Unit 2 No. 6 Alam Sutera Tangerang",
    "Outlet Slipi",
    "Jl. S. Parman Kav 29 A Jakarta Barat"
   ]
  },
  {
   "title": "Diskon 10% untuk Paket Big Box dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/085f0e6c028f61994d1f3fb4f4878799.jpg",
   "merchantName": "Pizza Hut Delivery",
   "validUntil": "24 January 2017",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1145",
   "content": "Undangan kerabat untuk pesta Pizza Big Box dengan Kartu Kredit BNI. Ketentuan:<br><br>\n-&#xA0; Diskon 10% untuk paket Big Box (Tanpa Minimum Transaksi)<br><br>\n-&#xA0; Berlaku untuk semua Kartu Kredit BNI termasuk Co Branding Garuda Card kecuali Corporate Card, Hasanah Card<br><br>\n-&#xA0; Diskon berlaku untuk maksimum 4 (empat) paket Big Box<br><br>\n-&#xA0; Promo berlaku di Setiap Hari (Senin - Minggu) termasuk Hari Libur Nasional<br><br>\n-&#xA0; Promo berlaku untuk pemesanan melalui seluruh channel PHD<br><br>\n-&#xA0; Berlaku di seluruh outlet PHD di Indonesia<br><br>\n-&#xA0; Tidak berlaku split bill<br><br>\n-&#xA0; Promo tidak dapat digabungkan dengan promo lainnya yang sedang berlangsung",
   "image": "http://m.bnizona.com//files/6965128992edf84ffdcb502b1fa47a0c.jpg",
   "phone": "021500600",
   "location": []
  },
  {
   "title": "Hemat 15% dengan BNI Reward Point di Pancious",
   "logoImage": "http://m.bnizona.com/files/3f49b1314cdf6d6b78e7343f7059ddee.jpg",
   "merchantName": "Pancious",
   "validUntil": "12 July 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1144",
   "content": "Manfaatkan BNI Reward Point dari Kartu Kredit BNI dengan santap bersama keluarga di Pancious.<br><br>\nKetentuan:<br><br>\n- Diskon sebesar 15% total bill dengan BNI Reward Point.<br><br>\n- Minimum transaksi Rp. 200.000 dan maksimum transaksi Rp. 1.500.000,- (before tax &amp; service)<br><br>\n- Promo diskon tidak berlaku untuk set menu dan minuman beralkohol.<br><br>\n- Tidak berlaku split bill dan tidak dapat digabungkan dengan promo lainnya<br><br>\n- Promo hanya berlaku untuk <em>dine in</em><br><br>\n- Berlaku untuk seluruh Kartu Kredit BNI kecuali Corporate Card dan iB Hasanah Card",
   "image": "http://m.bnizona.com//files/3efe7bbe5be8446ee45e3a0d2a1376ef.jpg",
   "location": [
    "http://www.pancious.com/",
    "Pancious Pacific Place | Pancious Plaza Indonesia | Pancious Grand Indonesia | Pancious Senayan City | Pancious Pondok Indah Mall | Pancious Kemang Colony | Pancious Mall Taman Anggrek | Pancious Emporium Pluit Mall | Pancious Central Park | Pancious Kelapa Gading | Pancious Sumarecon Mall | Pancious Serpong | Pancious Paris Van Java Bandung | Pancious Ciputra World Surabaya | Pancious Tunjungan Plaza Surabaya | Pancious Trans Studio Mall Makasar | Pancious Mal Puri Indah"
   ]
  },
  {
   "title": "Free Puyo 2 Silky Dessert dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/3f019ec2d14455cfb8b087f3fd721d36.jpeg",
   "merchantName": "Puyo",
   "validUntil": "28 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1143",
   "content": "Menyambut Imlek, ada promo dari Puyo Silky Dessert untuk pengguna Kartu Kredit BNI.<br><br>\nKetentuan:<br><br>\n- Free 2 Silky Dessert setiap transaksi minimum Rp 75.000,-<br><br>\n- Promo tidak berlaku kelipatan<br><br>\n- Promo tidak berlaku untuk pembelian paket 12<br><br>\n- Promo berlaku setiap hari termasuk hari libur nasional<br><br>\n- Tidak berlaku split bill<br><br>\n- Berlaku untuk Kartu Kredit BNI kecuali Corporate Card dan iB Hasanah Card<br><br>\n- Tidak dapat digabungkan dengan promo lainnya<br><br>\n&#xA0;",
   "image": "http://m.bnizona.com//files/fa1cc378526ed8ddb70ab5a37a892988.jpg",
   "location": [
    "Gandaria City",
    "Kota Kasablanka",
    "Mal Kelapa Gading",
    "Living World",
    "Teras Kota Serpong",
    "Summarecon Serpong",
    "Central Park",
    "Bintaro Xchange",
    "Mal Taman Anggrek",
    "Puri Indah Mall",
    "Baywalk Mall",
    "Mal Alam Sutera",
    "Pondok Indah Mall",
    "Emporium Mall Pluit"
   ]
  },
  {
   "title": "Sajian istimewa di Lilipadi Bandung Diskon 25%",
   "logoImage": "http://m.bnizona.com/files/7d814c9c0345f9da15991bf3212fba13.jpg",
   "merchantName": "Lilipadi Bandung",
   "validUntil": "29 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1104",
   "content": "Mau makan enak dan dapat diskon 25% ? Gunakan Kartu Kredit BNI untuk transaksi di Lilipadi Bandung hingga 29 Februari 2016. Ketentuan:<br>\n<ul><br>\n\t<li>Diskon 25% untuk Food &amp; Beverages</li><br>\n\t<li>Minimal transaksi Rp 150,000 dan maksimal transaksi Rp 1,5 juta</li><br>\n\t<li>Tidak berlaku untuk Take away, alcohol ataupun rokok</li><br>\n\t<li>Promo tidak dapat digabungkan dengan program promo lainnya yang sedang berlangsung</li><br>\n\t<li>Berlaku untuk semua jenis Kartu Kredit BNI kecuali Corporate Card dan Hasanah Card.</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/a294d2fc420dc4cf568f5eacb8d5cc68.jpg",
   "location": [
    "Jl. Talaga Bodas No.54, Bandung"
   ]
  },
  {
   "title": "Kuliner hemat di Blackpepper Bandung Diskon 30%",
   "logoImage": "http://m.bnizona.com/files/a6c0d8e8c728062b83d8326e6a2ce748.jpg",
   "merchantName": "Blackpepper Bandung",
   "validUntil": "29 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1102",
   "content": "Liburan semakin lengkap dengan kuliner hemat di Blackpepper Bandung. Diskon 30% untuk transaksi dengan Kartu Kredit BNI. Ketentuan:<br>\n<ul><br>\n\t<li>Diskon 30% untuk Food &amp; Beverages</li><br>\n\t<li>Minimal transaksi Rp 200,000 dan maksimal transaksi Rp 1,5 juta</li><br>\n\t<li>Tidak berlaku untuk Take away, alcohol ataupun rokok</li><br>\n\t<li>Promo tidak dapat digabungkan dengan program promo lainnya&#xA0;yang sedang berlangsung</li><br>\n\t<li>Berlaku untuk semua jenis Kartu Kredit BNI kecuali Corporate Card dan Hasanah Card</li><br>\n\t<li>Promo berakhir pada 29 Februari 2016.</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/4dce4059c493e2c937d8fc5c271ed868.jpg",
   "location": [
    "Jl. Maulana Yusuf No.10, Bandung"
   ]
  },
  {
   "title": "Diskon 20% dengan Kartu Kredit BNI @Excelso",
   "logoImage": "http://m.bnizona.com/files/159eae3c2f9b882ce285a4d2640b93ed.jpg",
   "merchantName": "Excelso",
   "validUntil": "10 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1081",
   "content": "<strong>Syarat &amp; Ketentuan</strong>:<br>\n<ul><br>\n\t<li>DISKON 20%&#xA0; (<em>food &amp; beverages</em>) untuk minimum transaksi Rp250.000,- dan maksimum transaksi Rp1.500.000,-</li><br>\n\t<li>Promo berlaku untuk&#xA0;<em>dine in&#xA0;</em>(tidak berlaku&#xA0;<em>take away</em>).</li><br>\n\t<li>Promo tidak berlaku untuk set menu dan minuman beralkohol.</li><br>\n\t<li>Promo berlaku setiap hari termasuk hari libur nasional.</li><br>\n\t<li>Tidak berlaku&#xA0;<em>split bill.</em></li><br>\n\t<li>Tidak dapat digabungkan dengan promo lainnya yang sedang berlangsung.</li><br>\n\t<li>Berlaku untuk Kartu Kredit BNI Titanium, Platinum, Co Branding Garuda dan Infinite kecuali Kartu Kredit BNI Silver, Gold, Corporate Card dan iB Hasanah.</li><br>\n\t<li>Periode promo hingga 10 Maret 2016</li><br>\n</ul><br>\nInfo lengkap hubungi BNI Call di 1500046",
   "image": "http://m.bnizona.com//files/3cd644a1e08596301cfda3363dda1478.jpg",
   "location": [
    "http://excelsocoffee.com/cafe/"
   ]
  },
  {
   "title": "HEMAT Rp10.000,- PAKET NASIKU (PANASKU) DI 7ELEVEN",
   "logoImage": "http://m.bnizona.com/files/cfabf057560e5cbfd6fc62e988864c55.jpg",
   "merchantName": "BNI Experience",
   "validUntil": "28 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/1076",
   "content": "Exclusive MasterCard Debit di semua outlet 7-Eleven<br><br>\n- Berlaku untuk pembelian paket Panasku di seluruh outlet 7-Eleven dengan menggunakan BNI Debit berlogo MasterCard.<br><br>\n- Berlaku kelipatan, maksimal 5 paket / transaksi / kartu.<br><br>\n- Tidak dapat digabungkan dengan program lain.<br><br>\n- Promo hingga 28 February 2016",
   "image": "http://m.bnizona.com//files/427ddcfe295cc66fdced30976f973799.jpg",
   "location": [
    "bniexperience@outlook.com"
   ]
  },
  {
   "title": "Fiddleheads Pondok Indah Mall Disc. up to 33%",
   "logoImage": "http://m.bnizona.com/files/cf5664d83dd9689dfa07419e9dbe2454.jpeg",
   "merchantName": "Fiddleheads",
   "validUntil": "30 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/967",
   "content": "- Diskon sebesar 33% food only di Weekdays (Senin - Jumat)<br><br>\n- Diskon sebesar 25%&#xA0; food only di Weekend (Sabtu &amp; Minggu) dan hari libur Nasional dan hari yang telah ditentukan (15 Mei; 1, 29, 30 Juni; 1 - 16&#xA0; July; 25 September; 23 - 24, 28 - 31 Desember 2015)<br><br>\n- Min. transaksi Rp. 350.000,- dan max. transaksi Rp. 1.500.000,- (sebelum diskon, pajak, dan service)<br><br>\n- Promo berlaku untuk dine in (tidak berlaku take away)<br><br>\n- Berlaku untuk Kartu Kredit BNI Style Titanium, Platinum &amp; Infinite (kecuali Kartu Kredit BNI Silver, Gold, Corporate, Co Brand Garuda dan Hasanah Card)<br><br>\n- Promo berakhir pada <strong>30 April 2016</strong>",
   "image": "http://m.bnizona.com//files/a7dc15342acbd32854991a13433e4c0a.jpg",
   "phone": "(021) 29529733",
   "location": [
    "Pondok Indah Mall, Street Gallery,",
    "Jl. Metro Pondok Indah, GF, Jakarta Selatan 12310"
   ]
  },
  {
   "title": "Enjoy dining at Incognito Disc. up to 33%",
   "logoImage": "http://m.bnizona.com/files/e06b803e9992b8d3cda6df4e81e4a71d.jpg",
   "merchantName": "Incognito",
   "validUntil": "30 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/966",
   "content": "- Diskon sebesar 33% food only di Weekdays (Senin - Jumat)<br><br>\n- Diskon sebesar 25% food only di Weekend (Sabtu &amp; Minggu) dan hari libur Nasional dan hari yang telah ditentukan (15 Mei; 1, 29, 30 Juni; 1 - 16&#xA0; July; 25 September; 23 - 24, 28 - 31 Desember 2015)<br><br>\n- Min. transaksi Rp. 200.000,- dan max. transaksi Rp. 1.500.000,- (sebelum diskon, pajak, dan service)<br><br>\n- Promo berlaku untuk dine in (tidak berlaku take away)<br><br>\n- Promo tidak dapat digabungkan dengan promo lainnya yang sedang berlangsung<br><br>\n- Berlaku untuk Kartu Kredit BNI Style Titanium, Platinum &amp; Infinite (kecuali Kartu Kredit BNI Silver, Gold, Corporate, Co Brand Garuda dan Hasanah Card)<br><br>\n- Promo berakhir pada <strong>30 April 2016</strong>",
   "image": "http://m.bnizona.com//files/43c3a5ed5b2963f9b06e5eee66682489.jpg",
   "phone": "(021) 29053068",
   "location": [
    "Gandaria City Mall",
    "Jl. Sultan Iskandar Muda No.33,",
    "Jakarta Selatan 12240"
   ]
  },
  {
   "title": "White Hunter Diskon hingga 33%",
   "logoImage": "http://m.bnizona.com/files/a29d5a042316b93a0725aa99a1975742.jpg",
   "merchantName": "White Hunter",
   "validUntil": "30 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/963",
   "content": "- Diskon sebesar 33% food only di Weekdays (Senin - Jumat)<br><br>\n- Diskon sebesar 25%&#xA0; food only di Weekend (Sabtu &amp; Minggu) dan hari libur Nasional dan hari yang telah ditentukan (15 Mei; 1, 29, 30 Juni; 1 - 16&#xA0; July; 25 September; 23 - 24, 28 - 31 Desember 2015)<br><br>\n- Min. transaksi Rp. 350.000,- dan max. transaksi Rp. 1.500.000,- (sebelum diskon, pajak, dan service)<br><br>\n- Promo berlaku untuk dine in (tidak berlaku take away)<br><br>\n- Promo tidak dapat digabungkan dengan promo lainnya yang sedang berlangsung<br><br>\n- Berlaku untuk Kartu Kredit BNI Style Titanium, Platinum &amp; Infinite (kecuali Kartu Kredit BNI Silver, Gold, Corporate, Co Brand Garuda dan Hasanah Card)<br><br>\n- Promo berakhir pada <strong>30 April 2016</strong>",
   "image": "http://m.bnizona.com//files/15cba32bf46e169cbe369669bd79d0e5.jpg",
   "phone": "(021) 2905 3018",
   "location": [
    "Gandaria City Unit 30, Jakarta (021) 2905 3018",
    "Living World Alam Sutera GF, Unit G  37 &amp; 37 A, Serpong (021) 5312 5496",
    "Emporium Pluit Mall GF Unit G 3536, Jakarta (021) 2907 1925"
   ]
  },
  {
   "title": "Han Gang Diskon hingga 33%",
   "logoImage": "http://m.bnizona.com/files/1df466566de3fdebb53e86addecc1427.jpg",
   "merchantName": "Han Gang",
   "validUntil": "30 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/962",
   "content": "- Diskon sebesar 33% food only di Weekdays (Senin - Jumat)<br><br>\n- Diskon sebesar 25%&#xA0; food only di Weekend (Sabtu &amp; Minggu) dan hari libur Nasional dan hari yang telah ditentukan (15 Mei; 1, 29, 30 Juni; 1 - 16&#xA0; July; 25 September; 23 - 24, 28 - 31 Desember 2015)<br><br>\n- Min. transaksi Rp. 350.000,- dan max. transaksi Rp. 1.500.000,- (sebelum diskon, pajak, dan service)<br><br>\n- Promo berlaku untuk dine in (tidak berlaku take away)<br><br>\n- Promo tidak dapat digabungkan dengan promo lainnya yang sedang berlangsung<br><br>\n- Berlaku untuk Kartu Kredit BNI Style Titanium, Platinum &amp; Infinite (kecuali Kartu Kredit BNI Silver, Gold, Corporate, Co Brand Garuda dan Hasanah Card)<br><br>\n- Promo berakhir pada <strong>30 April 2016</strong>",
   "image": "http://m.bnizona.com//files/53b671392848453a2702c617d7da39bc.jpg",
   "phone": "+62 21 23580710",
   "location": [
    "Senayan City LG Floor Unit L 09 A , Jakarta (021) 7278 1231",
    "Pacific Place Lt 5 unit 37, Jakarta (021) 5797 3460",
    "Mall Taman Anggrek Lt 4, Jakarta (021) 5639 499",
    "Grand Indonesia Sky Bridge Level 3 A Shop Unit FD 1  08, Jakarta (021) 2358 0710",
    "Jl. Wolter Monginsidi No 99 , Jakarta (021) 7278 7802",
    "Living World Alam Sutera GF, Unit G  37 &amp; 37 A",
    "Serpong (021) 5312 5496",
    "Emporium Pluit Mall GF Unit G 3536, Jakarta (021) 290 71925",
    "Plaza Senayan, Food Court Lt.3, Shop Unit ID 329 C (021) 572 4802",
    "Pondok Indah Mall, Street Gallery Lt 1 No 107108, Jakarta",
    "Baywalk Pluit"
   ]
  },
  {
   "title": "Grand Suki diskon hingga 33%",
   "logoImage": "http://m.bnizona.com/files/9ca3491e3432942cf0fda60e13b1535f.png",
   "merchantName": "Grand Suki",
   "validUntil": "30 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/898",
   "content": "<ul><br>\n\t<li>Diskon sebesar<strong> 33%</strong> food only di <strong>Weekdays (Senin - Jumat)</strong></li><br>\n\t<li>Diskon sebesar <strong>25%</strong>&#xA0; food only <strong>di Weekend (Sabtu &amp; Minggu) </strong>dan hari Libur Nasional dan hari yang telah ditentukan (15 Mei; 1, 29, 30 Juni; 1 - 16&#xA0; July; 25 September; 23 - 24, 28 - 31 Desember 2015)</li><br>\n\t<li>Min. transaksi Rp. 350.000,- dan max. transaksi Rp. 1.500.000,- (sebelum diskon, pajak, dan <em>service</em>)</li><br>\n\t<li>Berlaku untuk Kartu Kredit BNI Style Titanium, Platinum &amp; Infinite (kecuali Kartu Kredit BNI Silver, Gold, Corporate, Garuda BNI Platinum dan Hasanah Card)</li><br>\n\t<li>Promo berlaku untuk dine in (tidak berlaku <em>take away</em>)</li><br>\n\t<li>Promo tidak dapat digabungkan dengan promo lainnya yang sedang berlangsung</li><br>\n\t<li>Tidak berlaku split tagihan</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/9709036b9e653f18ec6d6a579b820791.jpg",
   "phone": "(021) 2358 1096",
   "location": [
    "Grand Indonesia Sky Bridge Level 5 Shop",
    "Unit FD 2  01, Jakarta"
   ]
  },
  {
   "title": "Diskon 20% di Beranda Cafe Crowne Plaza Hotel ",
   "logoImage": "http://m.bnizona.com/files/91c58a5ea341602bd6f78531a53aba05.gif",
   "merchantName": "Crowne Plaza Hotel",
   "validUntil": "10 June 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/18/882",
   "content": "<ul><br>\n\t<li>Berlaku untuk Kartu Kredit BNI kecuali Hasanah Card</li><br>\n\t<li>Berlaku hingga 10 Juni 2016</li><br>\n\t<li>Informasi lengkap hubungi BNI Call 1500046<br><br>\n\t&#xA0;</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/b0b5b14332997c332ad3748e9ca2e134.jpg",
   "phone": "+62 21 5268833",
   "location": [
    "Jln Gatot Subroto Kav. 23, Jakarta Selatan"
   ]
  }
 ],
 "Electronic": [
  {
   "title": "Installment 0% di Showroom Elektronik 88",
   "logoImage": "http://m.bnizona.com/files/3070dc904f9324d3c0f99accaa4490dc.jpg",
   "merchantName": "Showroom Elektronik 88",
   "validUntil": "23 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/835",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12 bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI (kecuali Hasanah Card dan Corporate Card)</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/0aadebf8655093118b08c87f9091330a.jpg",
   "location": [
    "Jl. Jend Sudirman Tembilahan"
   ]
  },
  {
   "title": "Installment 0% di Global Computer",
   "logoImage": "http://m.bnizona.com/files/88735f145c714cfee8f4e28aa2857456.jpg",
   "merchantName": "Global Computer",
   "validUntil": "05 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/827",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12 bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis kartu kredit BNI (kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/133c25cce168b8fa317a9258f5f467ab.jpg",
   "location": [
    "Jl. Hangtuah No.385 Dumai"
   ]
  },
  {
   "title": "Installment 0% 6 & 12 bulan di Global Electronic",
   "logoImage": "http://m.bnizona.com/files/3021a771c439709a14a0b97ca6065364.jpg",
   "merchantName": "Global Elektronik",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/791",
   "content": "<ul><br>\n\t<li>Installment&#xA0; 0 % tenor 6 &amp; 12 bulan</li><br>\n\t<li>Minimum Transaksi Rp. 500 ribu</li><br>\n\t<li>Periode hingga 31 Maret 2016</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/be8210b3e440b7b1881f9e49e601e04a.jpg",
   "location": [
    "JL MT HARYONO NO 509A SEMARANG",
    "JL PANDANARAN NO 102 SEMARANG",
    "JL FATMAWATI NO 3 SEMARANG",
    "RUKO SETIA BUDI BLOK A NO.6, JALAN SUKUN RAYA SEMARANG"
   ]
  },
  {
   "title": "Installment 0% di Toko Pluto Com",
   "logoImage": "http://m.bnizona.com/files/2aea935e938f0d46186551097946b642.jpg",
   "merchantName": "Toko Pluto Com",
   "validUntil": "22 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/754",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/eaa6f798975583e0551f40ea0d73b3ec.jpg",
   "location": [
    "Mal Pekanbaru, Jl. Jend Sudirman No.123 Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di Hongkong Elektronik",
   "logoImage": "http://m.bnizona.com/files/c903abb35cdc79d8d22d82b3ed37c887.jpg",
   "merchantName": "Hongkong Elektronik",
   "validUntil": "15 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/729",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/40d7a82b42fc2667865d5736c82050e6.jpg",
   "location": [
    "Jl. Imam Munandar No. 332 CD Pekanbaru",
    "Jl. Imam Munandar No. 310 PQ Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di Expo Elektronik",
   "logoImage": "http://m.bnizona.com/files/ba3aee05d7e688dc91f855ed8013a131.jpg",
   "merchantName": "Expo Elektronik",
   "validUntil": "19 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/725",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/12053b5ec8d5545fd5d42ed66bbdf96b.jpg",
   "location": [
    "Jl. DI Panjaitan KM 9 No.0910 Tanjung Pinang"
   ]
  },
  {
   "title": "Installment 0% di Emporium Collection",
   "logoImage": "http://m.bnizona.com/files/70ffee69355d7aafa3971011a6f65584.jpg",
   "merchantName": "Emporium Collection",
   "validUntil": "19 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/724",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/250c95911655589b7fd74b6f7f363fc2.jpg",
   "location": [
    "Komp windsor Central Batam"
   ]
  },
  {
   "title": "Installment 0% di Central Panam Elektronik",
   "logoImage": "http://m.bnizona.com/files/35e62690831e3ff2079e0fc9e0209280.jpg",
   "merchantName": "Central Panam Elektronik",
   "validUntil": "12 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/723",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/c2b28ea18af923228629fe8c33326824.jpg",
   "location": [
    "Jl.HR Subrantas, Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di Baby Power",
   "logoImage": "http://m.bnizona.com/files/b1f6ba1d05be48dfe5f212b8374c64a4.jpg",
   "merchantName": "Baby Power",
   "validUntil": "19 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/719",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/0ae48f994167bdd51cd03df26e3d08cc.jpg",
   "location": [
    "Komp Tanah Mas Blok D No.3 Sei Panas Batam"
   ]
  },
  {
   "title": "Installment 0% di Agung Jaya Elektronik",
   "logoImage": "http://m.bnizona.com/files/cbacb81ea74a1e7c9d8c12449d6cdcb2.jpg",
   "merchantName": "Agung Jaya Elektronik",
   "validUntil": "10 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/718",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/dabbf6b3fa07a563e4d31b7020126d65.jpg",
   "location": [
    "Jl. T. Tambusai No. 318 H Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di Abadi Elektronik",
   "logoImage": "http://m.bnizona.com/files/290a5129058ae4632d3e876d674c1d93.jpg",
   "merchantName": "Abadi Elektronik",
   "validUntil": "17 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/717",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/339752449fe97758129deea9116c4678.jpg",
   "location": [
    "Jl. Kaharuddin Nasution No. 22, Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di J-BROS",
   "logoImage": "http://m.bnizona.com/files/cfab55f222cbfc81f153c09bf391e6b1.jpg",
   "merchantName": "J-BROS",
   "validUntil": "09 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/19/690",
   "content": "<ul><br>\n\t<li>Installment 0% periode 3, 6 dan 12 bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/4093f54aad17ba0462c98a4a23295001.jpg",
   "location": [
    "Jl. AR Hakim Dalam Padang"
   ]
  }
 ],
 "Gadget": [
  {
   "title": "Launching Samsung Galaxy A Series 2016 by Erafone",
   "logoImage": "http://m.bnizona.com/files/908253088d22419c63ff424cb3fe7d0b.jpg",
   "merchantName": "Erafone",
   "validUntil": "28 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/20/1172",
   "content": "- Periode Program : 26 &#x2013; 28 Februari 2016<br><br>\n- Berlaku untuk semua jenis Kartu Kredit BNI kecuali Corporate Card &amp; iB Hasanah Card<br><br>\n- Lokasi : Atrium Senayan City<br><br>\n<strong>Syarat &amp; Ketentuan : </strong><br><br>\n1. Setiap pembelian Samsung Galaxy A Series A7 atau A5 mendapatkan Gimmick Samsung Level On&#xA0; (senilai Rp.2.850.000,-)<br><br>\n2. Setiap pembelian Samsung Galaxy A Series A3 mendapatkan Gimmick Samsung Level Box Mini (senilai Rp.1.350.000,-)<br><br>\n&#xA0;3. Pada saat Launching Samsung Galaxy A Series cashback hanya diberikan untuk produk Samsung tipe lainnya di luar Samsung A Series sebagai berikut :<br><br>\na. <em>Cashback</em> Rp 100 ribu setiap transaksi minimal Rp 1.499.000,- untuk 25 orang pertama dengan menggunakan Kartu Kredit BNI.<br><br>\nb. <em>Cashback</em> Rp 200 ribu setiap transaksi minimal Rp.3.999.000,- untuk 50 orang pertama dengan menggunakan Kartu Kredit BNI.<br><br>\nc. <em>Cashback</em> Rp 400 ribu setiap transaksi minimal Rp.6.999.000,- untuk 50 orang pertama dengan menggunakan Kartu Kredit BNI.<br><br>\nd. Dapatkan Gimmick menarik setiap pembelian Handphone Samsung tipe lainnya selama periode program<br><br>\n3. Berlaku BNI Installment 0% tenor 6,12 &amp; 18 bulan langsung di EDC BNI.",
   "image": "http://m.bnizona.com//files/cc46ef23143dab886639a1b47e74f79a.jpg",
   "location": [
    "Semua Outlet"
   ]
  },
  {
   "title": "CASHBACK hingga Rp250 ribu di Erafone Roadshow Supermall Karawaci",
   "logoImage": "http://m.bnizona.com/files/908253088d22419c63ff424cb3fe7d0b.jpg",
   "merchantName": "Erafone",
   "validUntil": "28 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/20/1168",
   "content": "- Periode Program : 22 - 28 Februari 2016<br><br>\n- Berlaku untuk semua jenis Kartu Kredit BNI kecuali Corporate Card &amp; iB Hasanah Card<br><br>\n- Lokasi : Supermall Karawaci Lt. Dasar<br><br>\n<strong>Syarat &amp; Ketentuan :</strong><br><br>\n1. <strong><em>Cashback </em>Rp150 ribu</strong> setiap transaksi minimal Rp2.000.000,- untuk 10 orang pertama dengan Kartu Kredit BNI.<br><br>\n2. <strong><em>Cashback </em>Rp250 ribu </strong>setiap transaksi minimal Rp4.000.000,- untuk 15 orang pertama dengan Kartu Kredit BNI.<br><br>\n3. <em>Cashback </em>digabungkan program BNI Installment 0% tenor 6 &amp; 12 bulan langsung di EDC BNI.<br><br>\n4. Syarat dan ketentuan lain berlaku.<br><br>\n&#xA0;",
   "image": "http://m.bnizona.com//files/51bddd2b4126586a2114e6fbf418a3ae.jpg",
   "location": [
    "Semua Outlet"
   ]
  },
  {
   "title": "Tambahan Cashback hingg Rp500 ribu di Erafone Lunar Fest 2016",
   "logoImage": "http://m.bnizona.com/files/908253088d22419c63ff424cb3fe7d0b.jpg",
   "merchantName": "Erafone",
   "validUntil": "29 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/20/1156",
   "content": "Periode Program : 04 - 29 Februari 2016<br><br>\nKartu Kredit : semua jenis Kartu Kredit BNI kecuali Corporate Card &amp; iB Hasanah Card<br><br>\n<strong>Syarat &amp; Ketentuan </strong>:<br><br>\n1. <em>Cashback </em>Rp150 ribu setiap transaksi minimal Rp2.499.000,- untuk 130 orang pertama dengan menggunakan Kartu Kredit BNI.<br><br>\n2. <em>Cashback </em>Rp300 ribu setiap transaksi minimal Rp5.000.000,- untuk 145 orang pertama dengan menggunakan Kartu Kredit BNI.<br><br>\n3. <em>Cashback </em>Rp400 ribu setiap transaksi minimal Rp7.000.000,- untuk 118 orang pertama dengan menggunakan Kartu Kredit BNI.<br><br>\n4. <em>Cashback </em>Rp500 ribu setiap transaksi minimal Rp10.000.000,- untuk 80 orang pertama dengan menggunakan Kartu Kredit BNI.<br><br>\n5. <em>Cashback </em>digabungkan program BNI Installment 0% tenor 6,12 &amp; 18 bulan langsung di EDC BNI.<br><br>\n6. Support <em>cashback </em>dari BNI sesuai realisasi penjualan selama periode promo.<br><br>\n7. Syarat dan ketentuan lain berlaku.",
   "image": "http://m.bnizona.com//files/9ff98886332502ff0c897945b4ece0ac.jpg",
   "location": [
    "Semua Outlet"
   ]
  },
  {
   "title": "Installment 0% di Link Cell Division",
   "logoImage": "http://m.bnizona.com/files/b93bdc03c09633a06b0de1ed33e489bf.jpg",
   "merchantName": "Link Cell Division",
   "validUntil": "19 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/20/828",
   "content": "<ul><br>\n\t<li>Installment 0% periode 3, 6 dan 12 bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000,- (lima ratus ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis kartu kredit BNI (kecuali Hasanah Card dan Corporate Card)</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/6e8a0740d6dcadfd9900fd651bbe914f.jpg",
   "location": [
    "Jl. Khatib Sulaiman Aur Tajungkang Bukittinggi",
    "Jl. Damar No.3435 Padang"
   ]
  },
  {
   "title": "Installment 0% di Smartphone Shop",
   "logoImage": "http://m.bnizona.com/files/28272597b7fb0044cf4019e506df626f.jpg",
   "merchantName": "Smartphone Shop",
   "validUntil": "12 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/20/741",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/645dea422103b3fa2e50c89f721fe2be.jpg",
   "location": [
    "Jl. Niaga No.146, Padang",
    "Jl.Pondok No.93 , Padang"
   ]
  },
  {
   "title": "Installment 0% di Oppo Store Centre",
   "logoImage": "http://m.bnizona.com/files/6496462ef88e5c414892f0ce792ff94a.jpg",
   "merchantName": "Oppo Store Centre",
   "validUntil": "18 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/20/736",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/f8ee08fc74ff0e01d8be6bd49fca487a.jpg",
   "location": [
    "Jl. Jend. Sudirman No.131 Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di Fernando ponsel",
   "logoImage": "http://m.bnizona.com/files/eb8186afc93bd2a7124f248d15a9c50e.jpg",
   "merchantName": "Fernando ponsel",
   "validUntil": "25 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/20/726",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/816b27efc86509211bf1465b0ff9ce21.jpg",
   "location": [
    "Jl. HR Subrantas No.7 Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di 7070 Selular",
   "logoImage": "http://m.bnizona.com/files/4e0d15504e42b1c5b2497ae760bc0589.jpg",
   "merchantName": "7070 Selular",
   "validUntil": "10 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/20/681",
   "content": "<ul><br>\n\t<li>Installment 0% periode 3, 6 dan 12 bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk.</li><br>\n\t<li>Berlaku semua jenis kartu kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/644ba2b09115db9e571618d6c3b76ff6.jpg",
   "location": [
    "7070 Selularshop, Jl. Thamrin No.34, Padang",
    "7070 Selular Pondok, Jl. Pondok No.5D, Padang",
    "7070 Selular Damar, Jl. Damar No.69A , Padang"
   ]
  }
 ],
 "Home": [
  {
   "title": "DISKON hingga 55% di AGM Fair - Mall Artha Gading    ",
   "logoImage": "http://m.bnizona.com/files/448300608916c94cb77f476d46c49196.jpg",
   "merchantName": "AGM - American Giant Mattress",
   "validUntil": "13 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/21/1170",
   "content": "- Periode program : hingga 13 Maret 2016&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;<br><br>\n- Berlaku untuk semua jenis Kartu Kredit BNI tidak termasuk Corporate Card &amp; iB Hasanah Card<br><br>\n- Venue : Atrium Italy Mal Artha Gading, Jakarta.<br><br>\n<strong>Syarat &amp; Ketentuan :</strong><br><br>\n1. Diskon hingga 55% + <em>Lucky dip </em>+ <em>Grand Prize </em>+ <em>Cashback </em>hingga Rp1 juta&#xA0; khusus transaksi menggunakan Kartu Kredit BNI.<br><br>\nBerikut tiering transaksi yang mendapatlkan cashback :<br><br>\n<img alt=\"\" src=\"/files/images/843b768941c9ffdaf002f28186cd4d1e.jpg\" style=\"height:74px; width:250px\"><br><br>\n2. Berkesempatan mendapatkan kupon <em>lucky dip </em>dengan minimal transaksi Rp7.500.000,- tidak berlaku kelipatan.<br><br>\n3. Berkesempatan mendapatkan kupon grand prize (TV, Kulkas, DVD, Kompor Gas) dengan minimal transaksi Rp7.500.000,- dan diundi pada tanggal 13 Maret 2016 jam 20.00 WIB.<br><br>\n4. Pemrosesan cashback akan dilakukan secara manual ke Kartu Kredit BNI maksimal 1 bulan setelah promo berakhir.<br><br>\n5. Promo berlaku untuk produk mattress King Koil, Serta, Florence, Tempur &amp; Stressless.<br><br>\n6. Berlaku cicilan 0% tenor 3, 6, 12 dan 18 bulan langsung di EDC BNI.<br><br>\n7. Syarat dan Ketentuan lain berlaku.",
   "image": "http://m.bnizona.com//files/ad1cb8793ab5805eec58c577525ce587.jpg",
   "location": [
    "http://www.americangiantmattress.com/"
   ]
  },
  {
   "title": "Diskon hingga 15% di KARE Design",
   "logoImage": "http://m.bnizona.com/files/f5dbf2f029c3764efb2ab8fa3ef0fb30.jpg",
   "merchantName": "KARE Design",
   "validUntil": "30 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/21/1164",
   "content": "<strong>- </strong>Periode program : 1 Februari 2016 s.d. 30 April 2016<br><br>\n- Lokasi : KARE 6th floor Senayan City Mall, Jakarta<br><br>\n<strong>Mekanisme :</strong><br><br>\n- Diskon 15% untuk produk terpilih khusus pengguna Kartu Kredit BNI Platinum &amp; Infinite.<br><br>\n- Diskon 10% untuk produk terpilih khusus pengguna Kartu Kredit BNI Silver, Gold &amp; Titanium.<br><br>\n- Tidak berlaku untuk Kartu Kredit BNI Corporate dan iB Hasanah.<br><br>\n- Promo tidak dapat digabung dengan promo lainnya.",
   "image": "http://m.bnizona.com//files/f10104bbac675e148150ffcc633d3db9.jpg",
   "location": [
    "https://www.karedesign.com/id/en/"
   ]
  },
  {
   "title": "Diskon hingga 20% untuk Produk Pilihan + Cicilan 0% hingga 12 bulan di ACE International Brands",
   "logoImage": "http://m.bnizona.com/files/4e12792ea3c17b20780e60018e783fd8.jpg",
   "merchantName": "ACE",
   "validUntil": "08 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/21/1159",
   "content": "- Periode : 10 Februari 2016 s.d. 08 Maret 2016<br><br>\n- Jenis Kartu : semua jenis Kartu Kredit BNI, Kartu Debit BNI &amp; iB Hasanah kecuali Corporate Card&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0; &#xA0;<br><br>\n- Outlet : semua outlet ACE Hardware Nasional&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0; &#xA0;<br><br>\n<strong>Syarat &amp; Ketentuan :</strong><br><br>\n1. Diskon spesial untuk produk terpilih khusus pengguna Kartu Kredit BNI, Kartu Debit BNI &amp; iB Hasanah Card yaitu :<br><br>\n- Diskon 20% untuk produk Baby Car Seat merk&#xA0; Sparco<br><br>\n- Diskon 20% untuk semua produk merk&#xA0; TATAY<br><br>\n- Hemat Rp. 250.000,- untuk produk kipas merk&#xA0; HUNTER<br><br>\n2.Berlaku BNI Installment 0% tenor 6 dan 12 bulan langsung di mesin EDC BNI untuk minimum transaksi sbb :<br><br>\na. 0% tenor 6 bulan minimum transaksi Rp 3.000.000,-<br><br>\nb. 0% tenor 12 bulan minimum transaksi Rp 8.000.000,-<br><br>\n3. Hemat hingga 50% atau maksimal Rp 5.000.000,- dengan menggunakan BNI Reward Points. Proses dilakukan langsung di mesin EDC BNI.<br><br>\n4. Syarat &amp; Ketentuan lainnya berlaku.",
   "image": "http://m.bnizona.com//files/eaeab27cd4839bd579b2042767473998.jpg",
   "location": [
    "Semua Outlet"
   ]
  }
 ],
 "Accessories": [
  {
   "title": "Diskon 15% + Cicilan 0% di Julia Jewelry",
   "logoImage": "http://m.bnizona.com/files/682d9ec77ff7a5cc1823f9248349d4c7.jpg",
   "merchantName": "Julia Jewelry",
   "validUntil": "28 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/22/1125",
   "content": "- Berlaku untuk pemegang Kartu Kredit &amp; Debit BNI.<br><br>\n- Diskon 15% + Cicilan 0% untuk pembelian<em> all item </em>+<em> gift </em>Boneka Teddy Bear dengan minimal pembelian Rp 3.000.000,-.<br><br>\n- Diskon 20% untuk pembelian barang <em>selected&#xA0; item</em> + <em>gift</em> Boneka teddy bear dengan minimal&#xA0;<br><br>\n- Mendapat kesempatan 2 kali untuk menebak besar karat&#xA0; berlian di dalam box berhadiah Liontin.<br><br>\n- Periode hingga 28 Februari 2016",
   "image": "http://m.bnizona.com//files/6aa6f65b0c854252c40a9d93d53f3a52.jpg",
   "location": [
    "Jakarta: Cibubur Junction, UG, Phone: (021) 87756527",
    "Serpong: Mall Alam Sutera, Lt 1, Phone: (021) 30449237",
    "Depok: Cimanggis Square, GF, Phone: (021) 29384579",
    "Serang: Mall Of Serang, UG, Phone: (0254) 8483768",
    "Bogor: Botani Square , GF, Phone: (0251) 8400814",
    "Cirebon: Grage Mall, Lt 2, Phone: (0231) 222948",
    "Yogyakarta: Malioboro Mall, GF, Phone: (0274) 586818",
    "Surabaya: Plaza Surabaya, Lt 1, Phone: (031) 5313045",
    "Sidoarjo: Mall Suncity , GF, Phone: (031) 8071089",
    "Kediri: Kediri Town Square, GF , Phone: (0354) 673845",
    "Malang: Malang Town Square, GF, Phone: (0341) 559195",
    "*Mall Olimpic Garden, Lt 1, Phone: (0341) 349195",
    "Bali: Discovery Shopping Mall, GF, Phone: (0361) 769677",
    "*: Mall Bali Galeria Dome, Lt 2, Phone: (0361) 767006",
    "Mataram: Mall Mataram, Lt 1, Phone: (0370) 631930",
    "Kupang: Flobamora Mall Kupang, GF, Phone: (0380) 840586",
    "Batam: Nagoya Hill Batam, Blok P #6, Phone: (0778) 7493858",
    "Jambi: Ruko Abadi Jambi, Blk A # 20, Phone: (0741) 24345",
    "Lampung: Central Plaza , Lt 1, Phone: (0721) 268515",
    "Palembang: Lippo Plaza Jakabaring, GF, Phone: (0711) 5649578",
    "* Palembang Trade Center, GF # C1&#x2013;58, Phone: (0711) 382138",
    "Samarinda: Samarinda Central Plaza , GF, Phone: (0541) 742601",
    "*Mall Lembuswana, GF, Phone: (0541) 205153",
    "*Plaza Mulia, GF, Phone: (0541) 7770902",
    "Pontianak: Mall Ahmad Yani, GF, Phone: (0561) 762004",
    "Balikpapan: Mall Fantasi, GF, Phone: (0542) 877464",
    "*Mall Balcony City, UG, Phone: (0542) 7582633",
    "Banjarmasin: Duta Mall, Lt 2, Phone: (0511) 4365333",
    "Kendari: Lippo Plaza Kendari, GF , Phone: (0401) 3196430",
    "Manado: Mega Mall, GF, Phone: (0431) 879800",
    "Jayapura: Mall Jayapura, GF, Phone:qw (0967) 5150047",
    "Ambon:Ambon City Center, Phone : ( 0911 ) 361440",
    "Pekanbaru:Mall SKA  lantai dasar # 39, Phone : 0761864320",
    "Julia Counter:",
    "* Sogo Kelapa Gading Mall 3, Lt 1, Jakarta,",
    "* Sogo Pondok Indah Mall 2, GF, Jakarta,",
    "* Sogo Emporium Pluit Mall, GF, Jakarta,",
    "* Sogo Central Park, GF, Jakarta,",
    "* Sogo Plaza Senayan, GF, Jakarta,",
    "* Lotte Shopping Avenue, UG, Jakarta,",
    "* Centro Margo City, GF, Depok,",
    "* Centro Bintaro Exchange, Lt. GF, Jakarta,",
    "* Centro Grand Metropolitan Mall, GF, Bekasi,",
    "* Sogo Paris Van Java, GF, Bandung,",
    "* Parkson Center Point, Lt. 1, Medan"
   ]
  },
  {
   "title": "Installment 0% di Toko Mido",
   "logoImage": "http://m.bnizona.com/files/679cc38fe1a7992865043dcfd2dbdd13.jpg",
   "merchantName": "Toko Mido",
   "validUntil": "01 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/22/753",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/8086c21498222b2c2866a01f13ffb2f9.jpg",
   "location": [
    "Mall SKA Lt II No.27 Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di Swiss Watch",
   "logoImage": "http://m.bnizona.com/files/9dc3c41f7d7979e043d7a27366d19695.jpg",
   "merchantName": "Swiss Watch",
   "validUntil": "19 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/22/744",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/ec3ce8b9b71e0900305a768e03c7db34.jpg",
   "location": [
    "Sari Jaya Hotel , Lt. dasar Blok A No.45 Nagoya Batam"
   ]
  },
  {
   "title": "Installment 0% di Swiss Collection",
   "logoImage": "http://m.bnizona.com/files/76ce3f398675347463cb2019b9dacfab.jpg",
   "merchantName": "Swiss Collection",
   "validUntil": "19 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/22/743",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/b6b1ed61ad63b8608fa11cfa74ec9795.jpg",
   "location": [
    "Komp Formosa Hotel Lt. Dasar Blok D No.1, Batam",
    "Komp Formosa Hotel Lt. Dasar Blok D No.3, Batam"
   ]
  }
 ],
 "Travel": [
  {
   "title": "DISKON TIKET PESAWAT Rp50 ribu + HOTEL 8% di TIKET.com",
   "logoImage": "http://m.bnizona.com/files/2d3edd6cf645fe42975f7eeda602c69f.png",
   "merchantName": "Tiket.com",
   "validUntil": "29 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/23/1137",
   "content": "- Booking Period : 1 &#x2013; 29 Februari 2016<br><br>\n- Berlaku untuk semua jenis Kartu Kredit BNI (Visa &amp; MasterCard)&#xA0;<strong>tidak&#xA0;termasuk</strong>&#xA0;Corporate Card dan iB Hasanah Card.<br><br>\n<strong>Syarat &amp; Ketentuan :</strong><br><br>\n-&#xA0;Potongan harga langsung Rp50.000,- untuk pemesanan tiket pesawat, berlaku untuk minimum transaksi Rp1.500.000,- (kecuali Garuda dan Citilink)<br><br>\n- Diskon 8% untuk pemesanan hotel tanpa minimum transaksi<br><br>\n- Berlaku untuk semua rute dan periode terbang dan berlaku untuk semua hotel yang berkerjasama dengan Tiket.com<br><br>\n- Pemesanan dapat dilakukan melalui Desktop:&#xA0;<a href=\"http://www.tiket.com/bni\">www.tiket.com/bni</a>&#xA0;mobile apps Tiket.com dan mobile link:&#xA0;<a href=\"http://m.tiket.com\">m.tiket.com</a>",
   "image": "http://m.bnizona.com//files/de7db25cad7480df2cacbc124fe0b055.jpg",
   "phone": " +62 21 8378 2020",
   "location": [
    "http://tiket.com"
   ]
  },
  {
   "title": "Spesial Imlek CITILINK dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/3afc77d361196938ea059948adfd0fb6.jpg",
   "merchantName": "Citilink",
   "validUntil": "28 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/23/1132",
   "content": "Periode Pemesanan: 20 Januari -&#xA0; 28 Februari 2016<br><br>\nPeriode Penerbangan: 20 Januari - 7 Maret 2016<br><br>\n<strong>Mekanisme:</strong><br><br>\n- Diskon Rp 75.000 berlaku untuk pemesanan tiket pesawat pulang dan pergi dengan Kartu Kredit BNI.<br><br>\n- Berlaku untuk semua rute penerbangan Citilink.<br><br>\n- Pemesanan langsung di website&#xA0;<a href=\"http://www.citilink.co.id\" target=\"_blank\">www.citilink.co.id</a> atau melalui Citilink mobile app.<br><br>\n- Khusus pemesanan melalui mobile apps, masukkan kode promo = 75ribu<br><br>\n<em>- Cashback </em>Rp 70.000, - dengan minimum transaksi Rp 1.500.000,- (<em>nett after discount</em>) berlaku khusus di hari Senin dan Kamis.<br><br>\n- Promo diskon dan cashback dapat digabungkan dengan program cicilan 0% dengan tenor 3 bulan.<br><br>\n- Berlaku untuk semua jenis Kartu Kredit BNI kecuali Corporate Card dan iB Hasanah Card.<br><br>\n- Setiap Kartu Kredit BNI hanya berhak mendapatkan satu kali cashback selama periode program.<br><br>\n- Cashback akan di proses ke Kartu Kredit CH maksimal 3 minggu setelah periode promo selesai.<br><br>\nInformasi lebih lengkap hubungi BNI Call 1500046 atau 68888 melalui ponsel.",
   "image": "http://m.bnizona.com//files/a5e82705a5e3c0e69e93b016b072f6f2.jpg",
   "phone": "0804 1 08 08 08",
   "location": [
    "www.citilink.co.id"
   ]
  },
  {
   "title": "Diskon Rp 50 ribu untuk pemesanan tiket pesawat di Ezytravel.co.id",
   "logoImage": "http://m.bnizona.com/files/99cdd850ef0e96a3c0ffc435ca216075.jpg",
   "merchantName": "EzyTravel",
   "validUntil": "29 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/23/1130",
   "content": "Persiapkan liburan tahun ini dengan diskon Rp 50 ribu dari Kartu Kredit BNI untuk pemesanan melalui Ezytravel.co.id<br><br>\n<strong>Ketentuan:</strong><br>\n<ul><br>\n\t<li>Diskon Rp 50.000 dengan Kartu Kredit BNI untuk pemesanan tiket pesawat di <a href=\"http://www.ezytravel.co.id\" target=\"_blank\">Ezytravel.co.id</a>.</li><br>\n\t<li>Berlaku hanya di hari <strong>Senin pukul 13.00 - 15.00 WIB</strong>.</li><br>\n\t<li>Berlaku untuk semua rute dan semua maskapai.</li><br>\n\t<li>Diskon berlaku tanpa minimum transaksi.</li><br>\n\t<li>Berlaku untuk semua jenis Kartu Kredit&#xA0; BNI kecuali Corporate Card dan iB Hasanah Card.</li><br>\n\t<li>Pemesanan dilakukan melalui <a href=\"http://www.ezytravel.co.id/tiket-pesawat\" target=\"_blank\">www.ezytravel.co.id/tiket-pesawat</a></li><br>\n\t<li>Tidak dapat digabungkan dengan promo lain.</li><br>\n\t<li>Periode pemesanan hingga 29 Februari 2016.</li><br>\n</ul><br>\nInformasi lebih lanjut hubungi BNI Call 1500046",
   "image": "http://m.bnizona.com//files/cb1a0ac1410ab118053b0306d8f7f6da.jpg",
   "phone": "1500833",
   "location": [
    "Jl. K.H. Samanhudi No. 22 C Pasar Baru,",
    "Sawah Besar  Jakarta Pusat, Indonesia 10710"
   ]
  },
  {
   "title": "Fly with Emirates Airlines disc. up to 15%",
   "logoImage": "http://m.bnizona.com/files/75742142f68a22a5a691233f52243f43.jpg",
   "merchantName": "Emirates Airlines",
   "validUntil": "30 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/23/954",
   "content": "- Diskon 15% untuk keberangkatan dari Denpasar.<br><br>\n- Diskon 10% untuk keberangkatan dari Jakarta.<br><br>\n- Hanya berlaku untuk Economy &amp; Business Class.<br><br>\n- Berlaku untuk seluruh destinasi Emirates Airlines.<br><br>\n- Berlaku untuk pembelian tiket secara online melalui landing page yang telah disediakan yaitu www.emirates.com/id/bni<br><br>\n- Diskon juga berlaku untuk pembelian tiket secara online melalui website www.emirates.com dengan memasukkan Tour<br><br>\n- Code <strong>IDBNI15</strong> pada saat&#xA0; melakukan pembayaran.<br><br>\n- Berlaku hingga <strong>30 Maret 2016</strong> untuk semua Jenis Kartu Kredit BNI kecuali Corporate Card dan Hasanah Card",
   "image": "http://m.bnizona.com//files/73a0cf7337e869bda6278b814f592591.jpg",
   "location": [
    "www.emirates.com"
   ]
  },
  {
   "title": "Sriwijaya Air Cicilan 0% 3, 6 & 12 Bulan pada Online dan Ticketing Office",
   "logoImage": "http://m.bnizona.com/files/bced1f73777c7b1de7b3ef9021b8af23.jpg",
   "merchantName": "Sriwijaya Air",
   "validUntil": "15 June 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/23/907",
   "content": "<ul><br>\n\t<li>Periode promo hingga 15 Juni 2016</li><br>\n\t<li>Berlaku untuk semua Jenis Kartu Kredit BNI kecuali Corporate Card dan Ib Hasanah Card</li><br>\n</ul><br>\n<strong>Syarat &amp; Ketentuan:</strong><br>\n<br>\n<ul><br>\n\t<li>Program Installment 0% 3, 6 dan 12 bulan.</li><br>\n\t<li>Berlaku untuk pembelian tiket secara online pada website <strong><a href=\"http://www.Sriwijayaair.co.id\">www.Sriwijayaair.co.id</a></strong> maupun pada Ticketing Office.</li><br>\n\t<li>Untuk cicilan 0% (3 &amp; 6 bulan) minimum transaksi Rp500.000,-</li><br>\n\t<li>Untuk cicilan 0% (12 bulan) minimum transaksi Rp1.000.000,-</li><br>\n\t<li>Berlaku BNI Reward Points (Konversi 1 point = Rp. 5,-). Hemat 50% atau maksimum Rp1.000.000,-. Hanya berlaku di Ticketing Office.</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/f2eec26293949c83779d761210cd6338.jpg",
   "location": [
    "Semua outlet"
   ]
  }
 ],
 "Hotel": [
  {
   "title": "Khusus Selasa Diskon Rp 100 ribu untuk order Hotel di Ezytravel.co.id",
   "logoImage": "http://m.bnizona.com/files/99cdd850ef0e96a3c0ffc435ca216075.jpg",
   "merchantName": "EzyTravel",
   "validUntil": "31 May 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/1117",
   "content": "<ul><br>\n\t<li>Diskon Rp 100.000 untuk pemesanan hotel domestik dan internasional</li><br>\n\t<li>Periode pemesanan Hotel : 1 Desember 2015 - 31 Mei 2016</li><br>\n\t<li>Pemesanan dilakukan hanya setiap hari <strong>Selasa</strong></li><br>\n\t<li>Diskon berlaku untuk pemesanan hotel domestik dan internasional untuk minimum transaksi Rp.1.000.000,-.</li><br>\n\t<li>Pemesanan dilakukan melalui <a href=\"http://www.ezytravel.co.id/hotel\" target=\"_blank\">www.ezytravel.co.id/hotel</a></li><br>\n\t<li>Tidak dapat digabungkan dengan promo lain</li><br>\n\t<li>Promo berlaku untuk setiap transaksi</li><br>\n\t<li>Berlaku untuk semua jenis Kartu Kredit&#xA0; BNI tidak termasuk Corporate Card kecuali iB Hasanah Card<br><br>\n\t<br><br>\n\t&#xA0;</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/88c69e10afa62c4a041b4c68b3617a77.jpg",
   "phone": "1500833",
   "location": [
    "Jl. K.H. Samanhudi No. 22 C Pasar Baru,",
    "Sawah Besar  Jakarta Pusat, Indonesia 10710"
   ]
  },
  {
   "title": "Diskon 10% untuk Kamar + 15% Resto & Spa di Solo Paragon Hotel",
   "logoImage": "http://m.bnizona.com/files/bca091de48a6d799695f19022136e783.jpg",
   "merchantName": "Solo Paragon Hotel",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/881",
   "content": "<ul><br>\n\t<li>Berlaku untuk Kartu Kredit BNI kecuali Hasanah Card</li><br>\n\t<li>Berlaku hingga 31 Maret 2016</li><br>\n\t<li>Info lebih lanjut hubungi BNI Call 1500046</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/3541d72a8dc2795f012ba2bf93445ebf.jpg",
   "phone": "+622717655888",
   "location": [
    "Jl. Dr. Soetomo,",
    "Solo 57125"
   ]
  },
  {
   "title": "Diskon 10% untuk Kamar + 15% Resto & Spa di Eden Hotel Bali",
   "logoImage": "http://m.bnizona.com/files/02d04f4ffd13ba2d560df33ad3af8db0.jpg",
   "merchantName": "Eden Hotel Bali",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/879",
   "content": "<ul><br>\n\t<li>Diskon 10% untuk Kamar</li><br>\n\t<li>Diskon 15% untuk Restauran &amp; Spa</li><br>\n\t<li>Berlaku untuk Kartu Kredit BNI kecuali Hasanah Card</li><br>\n\t<li>Berlaku hingga 31 Maret 2016</li><br>\n\t<li>Informasi lebih lanjut hubungi BNI Call 1500046</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/3623ae4ec70926c376cbef1ae5c8164a.jpg",
   "phone": "+62 361 3002121",
   "location": [
    "Jl. Kartika Plaza No. 42,",
    "Kuta, Bali 80361"
   ]
  },
  {
   "title": "Diskon 30% di Sagan Hotel - Yogyakarta",
   "logoImage": "http://m.bnizona.com/files/f70599d2c5d5308f755945be06c60643.jpg",
   "merchantName": "Sagan Hotel",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/831",
   "content": "- Diskon 30% untuk semua Room&#xA0;&#xA0;<br><br>\n- Berlaku bagi pengguna Kartu Kredit BNI kecuali Hasanah dan Corporate Card",
   "image": "http://m.bnizona.com//files/5dfe80ed534837b205dc981eb9a4a80f.jpg",
   "phone": "+62 274 549963",
   "location": [
    "Jalan Kartini No.4 Yogyakarta"
   ]
  },
  {
   "title": "Diskon up to 50% Merapi Merbabu Hotel - Yogyakarta",
   "logoImage": "http://m.bnizona.com/files/6981a637d141606e8af3b15fb67150b8.jpg",
   "merchantName": "Merapi Merbabu Hotel ",
   "validUntil": "20 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/816",
   "content": "- Diskon 50% untuk semua Room (kecuali long weekend &amp; high season)<br><br>\n- Diskon 15% untuk semua menu di Telogo Puteri Restaurant (tidak berlaku untuk minuman beralkohol dan rokok)<br><br>\n- Berlaku bagi pengguna Kartu Kredit BNI kecuali Hasanah dan Corporate Card",
   "image": "http://m.bnizona.com//files/d776ed7926696b575219a291519e1e8a.jpg",
   "location": [
    "Jalan Raya Seturan Yogyakarta"
   ]
  },
  {
   "title": "Diskon up to 30% di Tickle Hotel - Yogyakarta",
   "logoImage": "http://m.bnizona.com/files/81c1774be98752902b0b9da5f88d455b.jpg",
   "merchantName": "Tickle Hotel Yogyakarta",
   "validUntil": "20 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/815",
   "content": "- Diskon 30% untuk semua Room (kecuali long weekend &amp; high season)<br><br>\n- Diskon 10% untuk room service &amp; Pacifiy Restaurant<br><br>\n- Berlaku bagi pengguna Kartu Kredit BNI kecuali Hasanah dan Corporate Card",
   "image": "http://m.bnizona.com//files/c1c8aa72edfaaf80988ec7ae4d800535.jpg",
   "location": [
    "Jalan Urip Sumoharjo No. 44 Yogyakarta"
   ]
  },
  {
   "title": "Diskon 45% di Indoluxe Hotel - Yogyakarta",
   "logoImage": "http://m.bnizona.com/files/586337d50217b9e0518471b7a821604c.jpg",
   "merchantName": "Indoluxe Hotel",
   "validUntil": "15 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/811",
   "content": "- Diskon 45% untuk Superior &amp; Deluxe Room Type<br><br>\n- Diskon 10% untuk Food and Beverage",
   "image": "http://m.bnizona.com//files/587d58a2c0096e88e792f0f647295dbd.jpg",
   "phone": "+62 274 8722388",
   "location": [
    "Jalan Palagan Tentara Pelajar No.106 Yogyakarta"
   ]
  },
  {
   "title": "Diskon up to 55% di Crystal Lotus Hotel - Yogyakarta",
   "logoImage": "http://m.bnizona.com/files/e936da0b749b2623720325d9aeb101f3.jpg",
   "merchantName": "Crystal Lotus Hotel",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/810",
   "content": "- Diskon 55% untuk Superior, Deluxe, Suite Room (kecuali long weekend &amp; high season)<br><br>\n- Diskon 15% untuk Food &amp; Beverage di Lotus Restaurant dan Crystal Coffe &amp; Bar (minimal transaksi Rp 150.000 dan maksimal transaksi Rp 1.500.000)",
   "image": "http://m.bnizona.com//files/521b8c80d95545f4e764407254ce70eb.jpg",
   "location": [
    "Jalan Magelang KM 5.2 Yogyakarta"
   ]
  },
  {
   "title": "Diskon hingga 35% di Nagoya Plasa Hotel",
   "logoImage": "http://m.bnizona.com/files/adf0f6bbece9982977e8a4466678398a.jpg",
   "merchantName": "Nagoya Plasa Hotel",
   "validUntil": "16 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/787",
   "content": "<ul><br>\n\t<li>Diskon 35% all room dari harga publish</li><br>\n\t<li>Diskon 10% all F&amp;B</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit dan Debit BNI ( kecuali Hasanah Card, Corporate Card dan Debit BNI Syariah )</li><br>\n\t<li>Tidak berlaku untuk harga promo lain</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/56b5f338c31539b830a997f65a6ac9f8.jpg",
   "phone": "0778 459888",
   "location": [
    "Jl. Imam Bonjol, Lubuk Baja Batam"
   ]
  },
  {
   "title": "Diskon hingga 55% di Premiere Hotel Pekanbaru",
   "logoImage": "http://m.bnizona.com/files/99e087fcd174e31fec535612d5312128.jpg",
   "merchantName": "Premiere Hotel Pekanbaru",
   "validUntil": "15 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/24/701",
   "content": "<ul><br>\n\t<li>Diskon 55% (lima puluh lima persen) room dari Harga Publish.</li><br>\n\t<li>Diskon 10% (sepuluh persen) all F&amp;B di outlet Premiere Lounge dan The Caf&#xE9; , minimal transaksi Rp.250.000,- (dua ratus lima puluh ribu rupiah).</li><br>\n\t<li>Diskon 10% (sepuluh persen) di outlet Devata Spa</li><br>\n\t<li>Diskon 20% (dua puluh persen) di outlet Fitness Center</li><br>\n\t<li>Berlaku untuk seluruh jenis Kartu Kredit dan Kartu Debit BNI kecuali iB Hasanah Card, Corporate Card dan Kartu Debit BNI Syariah</li><br>\n\t<li>Tidak berlaku untuk acara-acara Event Nasional seperti acara Munas</li><br>\n\t<li>Tidak berlaku untuk paket pesta dan wedding</li><br>\n\t<li>Diskon tidak berlaku pada tanggal 31 Desember 2015</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/df0ccc3f8a231932d086d9932dab358a.jpg",
   "location": [
    "Jl. Jend. Sudirman No.389 Pekanbaru"
   ]
  }
 ],
 "Health Service": [
  {
   "title": "Gratis Voucher at Watsons Indonesia",
   "logoImage": "http://m.bnizona.com/files/f3f2997fcd10c7361723aa1adc310e7c.jpg",
   "merchantName": "watsons",
   "validUntil": "08 July 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/25/1005",
   "content": "a. Gratis voucher Watsons Rp10.000, untuk transaksi minimal Rp100.000,- untuk pembelanjaan seluruh produk<br><br>\nb. Berlaku di seluruh outlet Watsons Indonesia<br><br>\nc. Berlaku kelipatan<br><br>\nd. Voucher hanya dapat digunakan untuk pembayaran produk on Brand Watsons<br><br>\ne. Berlaku untuk semua Kartu Debit BNI, kecuali Kartu Debit BNI Syariah<br><br>\nf. Berlaku hingga 8 Juli 2016",
   "image": "http://m.bnizona.com//files/4341d5b4aa691ce94b5f39bf521e69e5.jpg",
   "location": [
    "http://www.watsonsasia.com"
   ]
  },
  {
   "title": "RS Surya Husadha Badung & Denpasar Diskon 20% Room dan Diskon 10% Medical Check-up",
   "logoImage": "http://m.bnizona.com/files/354fcd3ef843122e928734a44f5095d5.jpg",
   "merchantName": "RS Surya Husadha Denpasar ",
   "validUntil": "01 April 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/25/941",
   "content": "<ul><br>\n\t<li>Diskon 20% untuk Room</li><br>\n\t<li>Diskon 10% untuk Medical Check-up</li><br>\n\t<li>Berlaku hingga 1 April 2016 dengan Kartu Kredit BNI kecuali Hasanah Card &amp; Corporate Card</li><br>\n\t<li>Informasi lebih lanjut hubungi BNI Call 1500046</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/c5e7622b750caae80ae213790592161f.jpg",
   "phone": "+62 361 233787",
   "location": [
    "Jl. Serangan No. 7, Denpasar, Bali"
   ]
  }
 ],
 "Miscellaneous": [
  {
   "title": "SUKUK NEGARA RITEL Seri SR008 - Investasikan dana Anda untuk masa depan anak Indonesia",
   "logoImage": "http://m.bnizona.com/files/f845be280c76043aba10fd47be51d975.jpeg",
   "merchantName": "BNI ",
   "validUntil": "04 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1167",
   "content": "Bagi Anda yang mengutamakan keamanan dalam berinvestasi, SR008 adalah pilihan yang paling tepat bagi Anda.<br><br>\n<strong>Pengertian Sukuk Negara Ritel</strong><br><br>\nSurat Berharga Syariah Negara Ritel (Sukuk Negara Ritel) adalah Surat Berharga Negara yang diterbitkan berdasarkan prinsip syariah sebagai bukti atas bagian penyertaan terhadap Aset Surat Berharga Syariah Negara, yang dijual kepada individu atau perseorangan Warga Negara Indonesia melalui Agen Penjual di Pasar Perdana dalam Negeri.<br><br>\nSukuk Negara Ritel diterbitkan dengan tujuan: diversifikasi sumber pembiayaan APBN, memperluas basis investor, memberikan alternatif instrumen ritel yang berbasis syariah, memberi kesempatan investor kecil untuk berinvestasi dalam instrumen pasar modal dan memperkuat pasar modal Indonesia dengan mendorong transformasi <em>saving-oriented society</em> menjadi <em>investment-oriented society</em>.<br><br>\nSukuk Negara Ritel diterbitkan dalam bentuk tanpa warkat yang dapat diperdagangkan di Pasar Sekunder, sehingga investor nantinya hanya akan memperoleh Konfirmasi Kepemilikan Surat Utang Negara yang diterbitkan oleh Kustodian Sentral Efek Indonesia (KSEI).<br><br>\n<strong>Struktur Sukuk Negara Ritel Seri SR008</strong><br><br>\n<img alt=\"\" src=\"/files/images/84119c0245a237c986b8c68cfc167d97.jpg\" style=\"height:334px; width:350px\"><br><br>\n<strong>Keuntungan Berinvestasi di SR008</strong><br><br>\n1. Kupon dan pokok dijamin oleh Undang-Undang.<br><br>\n2. Kupon lebih tinggi dari rata-rata tingkat bunga deposito Bank BUMN.<br><br>\n3. Kupon dibayarkan setiap bulan dengan tingkat bunga tetap sampai tanggal jatuh tempo.<br><br>\n4. Dapat diperdagangkan di Pasar Sekunder dan berpotensi memperoleh keuntungan (<em>capital gain).</em><br><br>\n5. Dapat dijaminkan kepada pihak lain.<br><br>\n6. Masyarakat dapat berpartisipasi dalam mendukung pembiayaan pembangunan nasional.<br><br>\n7. Tidak bertentangan dengan prinsip-prinsip syariah.<br><br>\n8. Tarif pajak yang dikenakan lebih rendah dibanding deposito.<br><br>\n<strong>Risiko Berinvestasi di SR008 dan Cara Mengatasinya</strong><br><br>\n1. <strong>Risiko gagal bayar (<em>default risk</em>)</strong> adalah risiko dimana investor tidak dapat memperoleh pembayaran dana yang dijanjikan oleh penerbit pada saat produk investasi jatuh tempo. Investasi pada Sukuk Negara Ritel <strong>terbebas dari risiko gagal bayar</strong> dijamin pokok dan imbalannya oleh Undang-Undang (Negara).<br><br>\n2. <strong>Risiko pasar (<em>market risk</em>)</strong> adalah potensi kerugian (<em>capital loss</em>) bagi investor apabila terjadi kenaikan tingkat bunga yang menyebabkan penurunan harga SR008 di Pasar Sekunder. <strong>Kerugian (<em>capital loss</em>) dapat terjadi apabila investor menjual SR008 di Pasar Sekunder sebelum jatuh tempo pada harga jual yang lebih rendah dari harga belinya</strong>. Risiko pasar dalam investasi Sukuk Negara Ritel dapat dihindari apabila investor tidak menjual Sukuk Negara Ritel sampai dengan jatuh tempo dan hanya menjual SR008 jika harga jual (pasar) lebih tinggi daripada harga beli setelah dikurangi biaya transaksi.<br><br>\n3. <strong>Risiko likuiditas (<em>liquidity risk</em>) </strong>adalah potensi kerugian apabila sebelum jatuh tempo&#xA0; Pemilik SR008 yang memerlukan dana tunai mengalami kesulitan dalam menjual SR008 di Pasar Sekunder pada tingkat harga (pasar) yang wajar. Apabila pemilik Sukuk Negara Ritel membutuhkan dana, pemilik dapat:<br><br>\n- Menjual kembali SR008 yang dimilikinya kepada Agen Penjual tempat investor membeli pada harga kuotasi yang berlaku.<br><br>\n- Menjadikan SR008 sebagai jaminan dalam pengajuan jaminan ke bank umum atau sebagai jaminan dalam transaksi efek di pasar modal.<br><br>\n<strong>Persyaratan Pembelian Sukuk Negara Ritel di pasar perdana (pada masa penawaran)</strong><br><br>\n1. Perseorangan, Warga Negara Indonesia dibuktikan dengan KTP.<br><br>\n2. Memiliki rekening dana di bank dan memiliki rekening surat berharga di <em>sub-registry</em><strong>.<br><br>\nProsedur pemesanan Sukuk Negara Ritel di pasar perdana melalui BNI</strong><br><br>\n1. Membuka rekening tabungan di BNI (jika belum memiliki)<br><br>\n2. Membuka rekening efek di BNI Securities melalui cabang BNI<br><br>\n3. Mengisi formulir aplikasi pemesanan<br><br>\n4. Menyerahkan foto kopi KTP yang masih berlaku<br><br>\n5. Menyediakan dana yang cukup sesuai jumlah pesanan.",
   "image": "http://m.bnizona.com//files/21a5a909e578a0f72a190f56f445ae93.jpg",
   "phone": "021500046",
   "location": [
    "Gedung BNI",
    "Jl. Jend. Sudirman Kav.1, Jakarta, 10220"
   ]
  },
  {
   "title": "CASHBACK Rp200 ribu di MATAHARIMALL dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/49ec44b01240fb2e821d559f8e9632e1.jpg",
   "merchantName": "Matahari Mall",
   "validUntil": "29 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1165",
   "content": "- Periode program : 10 s.d. 29 Februari 2016<br><br>\n- Berlaku untuk semua jenis Kartu Kredit BNI kecuali Kartu Kredit BNI JCB, iB Hasanah dan Corporate Card<br><br>\n<strong>Syarat &amp; Ketentuan :</strong><br><br>\n1. <em>Cashback </em>Rp200 ribu akan diberikan kepada customer yang bertransaksi minimal Rp3 juta/<em>basket size </em>dengan Kartu Kredit BNI di <a href=\"http://www.MatahariMall.com\" target=\"_blank\">www.MatahariMall.com</a><br><br>\n2. Setiap <em>Customer Name </em>hanya berhak mendapat 1 kali cashback/hari selama periode program.<br><br>\n3. <em>Cashback </em>akan diberikan kepada 25 <em>Customer Name </em>per hari.<br><br>\n4. <em>Cashback </em>akan diproses paling cepat 1 bulan setelah periode program berakhir.<br><br>\n5. Program <em>cashback </em>dapat digabungkan dengan <em>full payment </em>dan promo cicilan 0% hingga 12 bulan",
   "image": "http://m.bnizona.com//files/1ca7b9f87a9f3fd05fd46de2d896f1f0.jpg",
   "location": [
    "https://www.mataharimall.com/"
   ]
  },
  {
   "title": "Cashback hingga Rp 200 ribu di BLANJA.com dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/a9dbee45bb9f1319e6bd6ff552fff0c2.jpg",
   "merchantName": "blanja.com",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1161",
   "content": "Periode Program : hingga 31 Maret 2016<br><br>\n1. Setiap bertransaksi menggunakan Kartu Kredit BNI di Blanja.com :<br><br>\n- Transaksi minimal Rp 2 juta s.d. Rp 3.499 juta akan mendapatkan cashback Rp 100.000,-<br><br>\n- Transaksi minimal Rp 3.5 juta s.d Rp 4.999 juta akan mendapatkan cashback Rp 150.000,-<br><br>\n- Transaksi minimal Rp 5 juta keatas akan mendapatkan cashback Rp 200.000,-<br><br>\n2. Promo hanya berlaku untuk transaksi di hari Rabu dan Kamis selama periode program.<br><br>\n3. Berlaku diskon hingga 50% untuk selected <em>items</em>.<br><br>\n4. Cashback tidak berlaku kelipatan dan tidak berlaku split transaksi.<br><br>\n5. Transaksi dilakukan secara online melalui <strong><a href=\"http://www.blanja.com/kp/promo-cashback-BNI\" target=\"_blank\">www.blanja.com</a></strong><br><br>\n6. Ketentuan pengiriman barang mengikuti ketentuan yang ditetapkan oleh Blanja.com<br><br>\n- Syarat dan ketentuan berlaku.<br><br>\n&#xA0;",
   "image": "http://m.bnizona.com//files/2dffdda6bf6d5e3925b5ca8aaf6a92a9.jpg",
   "location": [
    "http://www.blanja.com/"
   ]
  },
  {
   "title": "Ekstra diskon 15% + Gratis CD Deluxe Maliq & Dessential di Blibli.com",
   "logoImage": "http://m.bnizona.com/files/e5037e24126064d09e70b9b6d3673e04.png",
   "merchantName": "blibli.com ",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1160",
   "content": "<strong>Belanja online lebih mudah dan hemat dengan BNI Debit Online</strong><br><br>\n- Periode program: 10 Februari - 31 Maret 2016<br><br>\n<strong>Syarat &amp; Ketentuan :</strong><br><br>\n- Ekstra diskon 15% berlaku dengan minimum pembelanjaan Rp 150.000,-.<br><br>\n- Nominal pembelanjaan akan otomatis terpotong 15% pada saat <em>customer</em> membayar menggunakan BNI Debit Online dan memasukkan kode voucher <strong>BNIDEBIT-15 </strong>di laman Pengiriman &amp; Pembayaran.<br><br>\n- Kode voucher berlaku untuk 1000 customer pertama selama periode program.<br><br>\n- Dapatkan CD Deluxe Maliq &amp; D&apos;Essential dengan cara me-reedem email berisikan promo ID di laman website Blibli.com. CD akan dikirim maksimal 3 hari kerja.<br><br>\n- Promo tidak dapat digabungkan dengan promo lainnya, kecuali promo <em>free shiping. </em> Informasi lebih lengkap hubungi BNI Call <strong>1500046 </strong>atau <strong>68888 </strong>melalui ponsel",
   "image": "http://m.bnizona.com//files/dffa09f9e05c22e0374c48d1a3c96e34.jpg",
   "phone": "08041871871",
   "location": [
    "www.blibli.com"
   ]
  },
  {
   "title": "DISKON TAMBAHAN 10% di www.MatahariMall.com dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/49ec44b01240fb2e821d559f8e9632e1.jpg",
   "merchantName": "Matahari Mall",
   "validUntil": "29 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1139",
   "content": "Periode program :<strong> setiap hari hingga 01 - 29 Februari 2016</strong><br><br>\nJenis Kartu : Semua jenis Kartu Kredit BNI <strong>kecuali</strong> Kartu Kredit BNI iB Hasanah dan Corporate Card<br><br>\n<strong>Syarat &amp; Ketentuan:</strong><br><br>\n1. <strong>Diskon 10% (maksimal Rp200 ribu) </strong>untuk transaksi minimal Rp1juta setelah diskon dengan Kartu Kredit BNI.<br><br>\n2. Promo diskon berlaku untuk pembayaran <em>full payment </em>dan promo cicilan 0% s.d. 12 bulan.<br><br>\n3. Diskon berlaku untuk semua produk di <a href=\"http://www.MatahariMall.com\" target=\"_blank\">www.MatahariMall.com</a><br><br>\n4. Program berlaku untuk 750 customer pertama selama periode program.",
   "image": "http://m.bnizona.com//files/9c42bbccea08a19e6635deb5283c33c8.jpg",
   "location": [
    "https://www.mataharimall.com/"
   ]
  },
  {
   "title": "No Surcharge dan MasterCard Weekend Offer di SPBU Pertamina",
   "logoImage": "http://m.bnizona.com/files/2cefdac152c82c5fac11fd1fc1f33e5a.jpg",
   "merchantName": "Pertamina",
   "validUntil": "28 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1110",
   "content": "Hemat bayar bahan bakar Pertamina dengan Kartu Kredit BNI MasterCard.<br><br>\nPeriode program :&#xA0;28 Desember 2015 - 28 Februari 2016<br><br>\nKetentuan:<br>\n<ul><br>\n\t<li>Program <strong>No Surcharge</strong> dengan menggunakan Kartu Kredit MasterCard setiap hari untuk pembelian semua jenis bahan bakar selama periode program</li><br>\n\t<li>Program MasterCard Weekend Offer <strong>FREE 1 Liter seharga Rp1,- </strong>berlaku setelah pembelian minimum Rp300ribu produk Pertamax Series (Pertamax, Pertamax Dex dan Pertamax Plus) dengan Kartu Kredit MasterCard setiap hari Sabtu dan Minggu.</li><br>\n\t<li>Program hanya berlaku di SPBU yg berpartisipasi di area Jawa dan Bali</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/5949a870bd07e95a83b88e1eb9fe0781.jpg",
   "location": [
    "SPBU Pertamina Region Jawa dan Bali"
   ]
  },
  {
   "title": "Nikmati kenyamanan layanan perbankan digital di BNI e-Branch",
   "logoImage": "http://m.bnizona.com/files/f845be280c76043aba10fd47be51d975.jpeg",
   "merchantName": "BNI ",
   "validUntil": "31 December 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1094",
   "content": "<strong>BNI e-Branch&#xA0;|&#xA0;Pondok Indah Mall 2 Lantai Dasar</strong><br><br>\nTelah dibuka BNI e-Branch, outlet BNI dengan konsep digital dimana Anda dapat membuka rekening sekaligus mendapatkan info terkait produk dan promo secara digital serta berinteraksi langsung dengan petugas BNI Call melalui Video Contact Center.<br><br>\nLayanan yang ada di BNI e Branch:<br><br>\n<strong>e-Brochure</strong><br><br>\nPerangkat gadget/tablet yang disediakan oleh BNI dan dapat digunakan nasabahuntuk mengetahui informasi seputar produk dan promo BNI maupun Perusahaan Anak BNI. Nasabah juga dapat menggunakan e-Brochure&#xA0; untuk mengisi e-form pembukaan rekening dan setorantunai.<br><br>\n<strong>Smart Queuing System (SQS)</strong><br><br>\nPerangkat yang digunakan untuk mengisi e-form dan memperoleh nomor antrian pembukaan rekening atau setoran perusahaan.<br><br>\n<strong>Self Service Terminal (SST)</strong><br><br>\nDapat digunakan nasabah untuk melakukan top up (pengisian) saldo Kartu Tap Cash BNI.<br><br>\n<strong>Banknote Deposit Machine (BDM)</strong><br><br>\nSarana/perangkat untuk melakukan setoran multi denominasi.<br><br>\n<strong>Interactive Wall</strong><br><br>\nMedia informasi berupa layar interaktif yang menampilkan informasi produk dan promo BNI serta perusahaan anak BNI.<br><br>\n<strong>Video Contact Center (VCC)</strong><br><br>\nSarana yang dapat digunakan nasabah untuk memperoleh informasi produk, menyampaikan complain/keluhan dan transaksi perbankan dengan dipandu oleh petugas BNI Call secara virtual.<br><br>\nInformasi lebih lanjut kunjungi BNI e-Branch di Pondok Indah Mall 2 lantai dasar atau hubungi <strong>BNI Call 1500046</strong>&#xA0; atau <strong>68888</strong> melalui ponsel atau kunjungi&#xA0;<strong><a href=\"http://www.bni.co.id/\" target=\"_blank\">www.bni.co.id</a></strong>",
   "image": "http://m.bnizona.com//files/09fb13e30d7e4df8b519cb82b4fd2d1a.jpg",
   "phone": "021500046",
   "location": [
    "Gedung BNI",
    "Jl. Jend. Sudirman Kav.1, Jakarta, 10220"
   ]
  },
  {
   "title": "Transaksi dengan PIN Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/f845be280c76043aba10fd47be51d975.jpeg",
   "merchantName": "BNI ",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1084",
   "content": "- Periode program : Desember 2015 s.d. 31 Maret 2016.<br><br>\n- Kartu BNI : berlaku untuk semua jenis Kartu Kredit BNI (Kecuali IB Hasanah Card).<br><br>\n<strong>Syarat &amp; Ketentuan: </strong><br><br>\n1. Program yang dijalankan merupakan program campaign untuk melakukan sosialisasi RPIN sekaligus mendorong penggunaan PIN Kartu Kredit BNI.<br><br>\n2. Program ini berlaku untuk transaksi yang dilakukan di EDC merchant selama periode program.<br><br>\n3. Cashback senilai Rp100.000,- diberikan kepada 50 (lima puluh) pemegang Kartu Kredit BNI dengan transaksi menggunakan PIN terbanyak setiap bulannya selama periode program.<br><br>\n4. <em>Cashback</em> akan diberikan jika pemegang kartu memenuhi syarat minimum transaksi menggunakan PIN sebanyak 3 kali dalam bulan tersebut tanpa dibatasi dengan nilai nominal tertentu.<br><br>\n5. <em>Cashback</em> akan dikreditkan ke dalam tagihan Kartu Kredit setelah pengumuman pemenang.<br><br>\n6. <em>Gimmick </em>dalam bentuk <em>Smartphone </em>akan diberikan kepada 3 (tiga) pemenang utama dengan akumulasi transaksi menggunakan PIN terbanyak selama periode program.<br><br>\n7. Rincian <em>cashback </em>dan <em>gimmick </em>yang akan diberikan selama periode program adalah sebagai berikut:<br><br>\n<img alt=\"\" src=\"/files/images/b9d767cd20d5c192a82263301a4ec56d.jpg\" style=\"height:142px; width:374px\">",
   "image": "http://m.bnizona.com//files/cc41b8ebb2d85aef4f224b556bba7ae1.jpg",
   "phone": "021500046",
   "location": [
    "Gedung BNI",
    "Jl. Jend. Sudirman Kav.1, Jakarta, 10220"
   ]
  },
  {
   "title": "BNI Twitter #Hashtag",
   "logoImage": "http://m.bnizona.com/files/cfabf057560e5cbfd6fc62e988864c55.jpg",
   "merchantName": "BNI Experience",
   "validUntil": "31 December 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1077",
   "content": "<strong>Cara menggunakan BNI Twitter #Hashtag : #AskBNI </strong><br><br>\n1. Follow Twitter @BNI46<br><br>\n2. Untuk info penggunaan, kirim Direct Message ke Twitter @BNI46 ketik #AskBNI. Akan ada reply via DM informasi cara penggunaannya.<br><br>\n3. Cara Penggunaan : (Kirim Direct Message) #Promo (spasi) #[Keyword] coontoh:<br><br>\na. #Promo #Hotel<br><br>\nb. #Promo #Travel<br><br>\nc. #Promo #eCommerce<br><br>\nUntuk mengetahui semua Keyword Promo kirim DM, ketik: <strong>#HelpPromo </strong><br><br>\n#AskBNI (spasi) #[Keyword] contoh:<br><br>\na. #AskBNI #Taplus<br><br>\nb. #AskBNI #TaplusBisnis<br><br>\nc. #AskBNI #DebitCard<br><br>\nUntuk mengetahui semua Keyword #AskBNI kirim DM, ketik:<strong> #HelpBNI</strong>",
   "image": "http://m.bnizona.com//files/f19beb57a297008b6fca69464b4cd4d9.jpg",
   "location": [
    "bniexperience@outlook.com"
   ]
  },
  {
   "title": "14-DAYS FREE Trial at English First",
   "logoImage": "http://m.bnizona.com/files/6edca6072af2292b40055aac2fe8c083.jpg",
   "merchantName": "English First",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1032",
   "content": "<strong>Periode program : 1 Oktober 2015 - 31 Maret 2016 (6 bulan)</strong><br>\n<ul><br>\n\t<li>14-DAYS FREE Trial untuk usia 18 tahun keatas dan professionals (hanya berlaku hingga Oktober 2015).</li><br>\n\t<li>FREE tambahan studi selama 3 bulan + Voucher MAP/Electronic Solution Rp 1juta&#xA0; Khusus untuk pemegang Kartu Kredit BNI yang mendaftarkan diri untuk paket 12 bulan (selama persediaan masih ada).</li><br>\n\t<li>Program hanya berlaku 4 lokasi EF English Centers FX Sudirman, Kuningan City, Mall Taman Anggrek dan Surabaya Town Square.</li><br>\n\t<li>Berlaku cicilan 0% dengan tenor 6 dan 12 bulan.</li><br>\n\t<li>Berlaku untuk Kartu Kredit BNI kecuali iB Hasanah dan Corporate Card.</li><br>\n</ul><br>\nInfo lebih lengkap hubungi BNI Call di 1500046<br><br>\n<strong>Outlet:</strong><br><br>\nEF English Centers &#x2013; FX Sudirman, F3 | EF English Centers &#x2013; Taman Anggrek Mall, L3 North | EF | English Centers &#x2013; Kuningan City | EF English Centers &#x2013; Surabaya Town Square<br><br>\n&#xA0;",
   "image": "http://m.bnizona.com//files/18d208803afad9c8527583aa16e26678.jpg",
   "phone": "0215206477",
   "location": [
    "Wisma Tamara, Lt. 4, Suite 403, Jl. Jend. Sudirman, Kav. 24, Jakarta 12920, Indonesia"
   ]
  },
  {
   "title": "Rejeki BNI Taplus 2015",
   "logoImage": "http://m.bnizona.com/files/f845be280c76043aba10fd47be51d975.jpeg",
   "merchantName": "BNI ",
   "validUntil": "31 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/26/1030",
   "content": "Ubah kebiasaan nggak ada poinnya menjadi poin. Menangkan dan bawa pulang Mercedes-Benz atau Honda HR-V atau Vespa Primavera yang keren banget dengan melakukan transaksi e-Banking dan tambah saldo terus di program Rejeki BNI Taplus, Ada Poinnya. Selain itu bisa bawa pulang hadiah langsungnya hanya menambah tabungan minimal Rp45 juta.<br><br>\nProgram ini berlaku sejak tanggal <strong>1 Oktober 2015 s.d. Maret 2016</strong> dan berlaku bagi nasabah BNI Taplus, BNI Taplus Bisnis, BNI Taplus Muda, BNI Taplus Anak dan BNI Emerald Saving.<br><br>\n<strong>Program Undian</strong><br><br>\nSemakin banyak poinnya, semakin besar kesempatan menangnya.<br><br>\nSetiap transaksi dapat 1 (satu) poin<br>\n<ul><br>\n\t<li>Transaksi e-Banking (ATM, SMS Banking, Internet Banking, Mobile Banking, Phone Banking) minimal Rp50 ribu,</li><br>\n</ul><br>\n- Bayar tagihan telepon, listrik, kartu kredit dan lain-lain<br><br>\n- Beli pulsa, tiket dan lain-lain<br><br>\n- Transfer<br>\n<ul><br>\n\t<li>Belanja pakai BNI Debit Card min. Rp50 ribu</li><br>\n\t<li>Pengendapan saldo rata-rata min. Rp1 juta/bulan</li><br>\n\t<li>Aktivasi e-Banking</li><br>\n\t<li>Billpayment (Autodebit)</li><br>\n</ul><br>\n<strong>Jenis hadiah dan periode undian<br><br>\n<img alt=\"\" src=\"/files/images/c32c211d8fd540dd8ea0a9415b27fb28.png\" style=\"height:148px; width:375px\"><br><br>\nKetentuan Program</strong><br>\n<br>\n<ul><br>\n\t<li>Satu nasabah berhak menang sekali dalam setiap periode undian.</li><br>\n\t<li>Nasabah yang sudah menang hadiah Periode Bulanan, memiliki kesempatan menang di periode Triwulanan maupun Grand Prize.</li><br>\n\t<li>Pajak Hadiah sebesar 25% dan biaya balik nama menjadi tanggungan pemenang.</li><br>\n\t<li>Warna hadiah sesuai persediaan.</li><br>\n\t<li>Hadiah yang tidak diambil atau klaim dalam waktu 30 hari akan diatur sesuai dengan ketentuan yang berlaku.</li><br>\n\t<li>Pemenang dapat melihat informasi melalui BNI Call 1500046 atau ke Customer Service di Cabang Pembuka Rekening.</li><br>\n\t<li>Informasi pemenang akan diumumkan melalui media cetak, website bni.co.id dan Kantor Cabang BNI terdekat.</li><br>\n</ul><br>\n<strong>Simulasi Perolehan poin </strong><br><br>\n<strong>Nasabah AA</strong><br>\n<br>\n<ul><br>\n\t<li>Aktivasi e-Banking (SMS Banking dan Internet Banking)</li><br>\n\t<li>Transaksi e-Banking pembayaran/pembelian/transfer 10 x dalam sebulan</li><br>\n\t<li>Belanja dengan BNI Debit Card 2 x dalam sebulan</li><br>\n\t<li>Saldo rata-rata Rp7,5 juta dalam sebulan</li><br>\n</ul><br>\nDalam Periode Bulanan, poin terkumpul sebagai berikut:<br><br>\n<img alt=\"\" src=\"/files/images/b53f2e9839b79be2db47c377045ff5fd.png\" style=\"height:85px; width:375px\"><br><br>\nNasabah AA berhak mengikuti program periode bulanan hadiah Vespa Primavera<br><br>\n<strong>Nasabah BB</strong><br>\n<br>\n<ul><br>\n\t<li>Aktivasi e-Banking (SMS Banking dan Internet Banking) di bulan ke-1</li><br>\n\t<li>Transaksi e-Banking pembayaran/pembelian/transfer 4 x di bulan ke-1, 5 x di bulan ke-2, 2 x di bulan ke-3</li><br>\n\t<li>Belanja dengan BNI Debit Card 2 x di bulan ke-1, 1 x di bulan ke-2, 2 x di bulan ke-3</li><br>\n\t<li>Saldo rata-rata Rp7,5 juta di bulan ke-1, Rp6 juta di bulan ke-2, Rp5 juta di bulan ke-3</li><br>\n</ul><br>\nDalam Periode Triwulanan, poin terkumpul sebagai berikut:<br><br>\n<img alt=\"\" src=\"/files/images/5de42cbc93632bbec715be8027ac2fd8.png\" style=\"height:162px; width:375px\"><br><br>\nNasabah BB berhak mengikuti program periode Triwulanan hadiah Honda HR-V.<br><br>\n<strong>Nasabah DD</strong><br>\n<br>\n<ul><br>\n\t<li>Aktivasi e-Banking - SMS Banking di bulan ke-1 dan Internet Banking di bulan ke-4</li><br>\n\t<li>Transaksi e-Banking pembayaran/pembelian/transfer 2 x di bulan ke-1, 2 x di bulan ke-2, 1 x di bulan ke-3, 5 x di bulan ke 5 dan 3 x di bulan ke 6</li><br>\n\t<li>Belanja dengan BNI Debit Card 3 x di bulan ke-1, 2 x di bulan ke-2, 5 x di bulan ke-3 , 4 x dibulan ke 4, 3 x dibulan ke 6</li><br>\n\t<li>Saldo rata-rata Rp15 juta di bulan ke-1, Rp12 juta di bulan ke-2, Rp20 juta di bulan ke-3, Rp50 juta di bulan ke-4, Rp 9 juta di bulan ke-5, Rp19 juta di bulan ke-6</li><br>\n</ul><br>\nDalam Periode Grand Prize, poin terkumpul sebagai berikut:<br><br>\n<img alt=\"\" src=\"/files/images/b604b984edb43498991f5359bba66a7d.png\" style=\"height:235px; width:375px\"><br><br>\nNasabah DD tidak berhak mengikuti program periode Grand Prize Mecedes-Benz C250 AMG<strong>.<br><br>\nProgram Hadiah Langsung</strong><br><br>\nDapatkan hadiah langsung Emas Murni 24K hanya dengan membuka tabungan atau menambah saldo minimal Rp45 juta dan bersedia diblokir saldonya.<br><br>\nPilih hadiahnya , tentukan jangka waktu blokirnya sebagaimana tabel dibawah:<br><br>\n<img alt=\"\" src=\"/files/images/ae90b685901cabd713567bc8cf3d0172.png\" style=\"height:210px; width:375px\"><br><br>\nCatatan:<br><br>\nNilai saldo blokir dapat berubah sewaktu-waktu sesuai dengan perkembangan fluktuasi harga emas di pasar.<br><br>\nProgram berlaku untuk nasabah perorangan BNI Taplus, BNI Taplus Bisnis, BNI Taplus Muda, BNI Taplus Anak atau BNI Emerald Saving",
   "image": "http://m.bnizona.com//files/d3ebb955e8eace65bdf0141d34092abc.jpg",
   "phone": "021500046",
   "location": [
    "Gedung BNI",
    "Jl. Jend. Sudirman Kav.1, Jakarta, 10220"
   ]
  },
  {
   "title": "BNI EXPERIENCE",
   "logoImage": "http://m.bnizona.com/files/cfabf057560e5cbfd6fc62e988864c55.jpg",
   "merchantName": "BNI Experience",
   "validUntil": "31 December 2020",
   "href": "http://m.bnizona.com/index.php/promo/view/26/983",
   "content": "BNI Experience adalah teman hidup anda yang membuat hidup anda lebih mudah! Melalui app ini, anda dapat menemukan ATM dan cabang BNI terdekat untuk memenuhi kebutuhan finansial layanan anda. Anda tidak akan lagi ketinggalan berbagai keuntungan sebagai nasabah BNI karena app ini selalu memberi tahu setiap promosi dan diskon dari merchant-merchant BNI. Bukan itu saja, BNI akan memberikan berbagai hadiah kejutan dari fitur &quot;Coupon&quot; yang dapat anda gunakan untuk mengakses banyak manfaat dari berbagai merchant. Nikmati pengalaman hidupmu dengan BNI Experience!<br><br>\n<br><br>\n<img alt=\"\" src=\"/files/images/ecf3a95944b15a954f7e531e04706c2d.png\" style=\"height:89px; width:300px\"><br><br>\natau klik <strong><a href=\"http://bit.ly/BNIX_Android\">disini</a></strong><br><br>\n<img alt=\"\" src=\"/files/images/2d82817c3ec14d267453a3a0c1f1d1f8.png\" style=\"height:89px; width:301px\"><br><br>\natau klik <strong><a href=\"http://bit.ly/BNIXiOS\">disini</a></strong><br><br>\n<img alt=\"\" src=\"/files/images/f0e02b718d71044958f6e0e58016a45d.png\" style=\"height:106px; width:300px\"><br><br>\natau klik <strong><a href=\"http://bit.ly/BNIX_BlackBerry\">disini</a></strong><br><br>\n&#xA0;",
   "image": "http://m.bnizona.com//files/8e20428cf6e48e2024b6674ea375ce56.jpg",
   "location": [
    "bniexperience@outlook.com"
   ]
  }
 ],
 "Entertainment": [
  {
   "title": "Langganan MNC Play Pay 8 Get Free 4 months atau Pay 4 Get Free 2 months ",
   "logoImage": "http://m.bnizona.com/files/5e18ed1489139812180601e653ff3038.jpg",
   "merchantName": "MNC Play",
   "validUntil": "14 June 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/28/961",
   "content": "<ul><br>\n\t<li>Promo berlaku untuk pelanggan baru dan yang sudah berlangganan MNC Play serta memiliki Kartu Kredit BNI</li><br>\n\t<li>Penawaran istimewa Pay 8 Get Free 4 months atau Pay 4 Get Free 2 months dapat digabungkan dengan Cicilan 0% selama 6 dan 12 bulan dengan minimum transaksi Rp 500.000,-</li><br>\n\t<li>Berlaku untuk semua Kartu Kredit BNI kecuali Corporate Card dan iB Hasanah Card</li><br>\n\t<li>Promo berakhir pada 14 Juni 2016</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/6201845f15a09c086ea897f8b3383cc6.jpg",
   "phone": "021  3911112",
   "location": [
    "MNC Tower, Lantai 10, 11, 12A,",
    "Jl. Kebon Sirih No. 1719, Gondangdia, Jakarta Pusat"
   ]
  }
 ],
 "Sports": [
  {
   "title": "Diskon hingga 15% brand Nike dengan Kartu Kredit BNI",
   "logoImage": "http://m.bnizona.com/files/fa99c38a91b79717219061b5966a0b58.jpg",
   "merchantName": "Nike",
   "validUntil": "18 March 2017",
   "href": "http://m.bnizona.com/index.php/promo/view/34/1116",
   "content": "- Diskon 10% untuk brand NIKE dengan Kartu Kredit BNI Silver &amp; Gold (Regular, Affinity &amp; Co-brand)<br><br>\n- Diskon 15% untuk brand NIKE dengan Kartu Kredit BNI Titanium, Platinum, Signature &amp; Infinite (Regular, Affinity &amp; Co-brand)<br><br>\n- Minimal transaksi Rp 2 juta/kartu<br><br>\n- Promo berakhir pada 18 Maret 2017",
   "image": "http://m.bnizona.com//files/54da17474bb62b4c5f25eb2e92f67fa6.jpg",
   "phone": "02123581085",
   "location": [
    "NIKE Emporium",
    "NIKE Grand Indonesia",
    "NIKE Pacific Place",
    "NIKE Kota Kasablanka",
    "NIKE Kemang Village",
    "NIKE Lotte Shopping Avenue",
    "NIKE FX Sudirman",
    "NIKE Baywalk Mall",
    "NIKE Bintaro Jaya Xchange",
    "NIKE Alam Sutera"
   ]
  }
 ],
 "Cicilan 0%": [
  {
   "title": "Installment 0% di VH Pusat Grosir",
   "logoImage": "http://m.bnizona.com/files/fdf07a02f1c8dd2bcc4f911cd43304f8.jpg",
   "merchantName": "VH Pusat Grosir",
   "validUntil": "12 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/35/758",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/8fee362d61aa92d9634c5f69b9548301.jpg",
   "location": [
    "Plaza Senapelan Lt. 1 No. A128 Jl. Teuku Umar No. 1, Pekanbaru"
   ]
  },
  {
   "title": "Installment 0% di Toko Maju",
   "logoImage": "http://m.bnizona.com/files/e8a4fde9679b7b9508394efa0b717ffe.jpg",
   "merchantName": "Toko Maju",
   "validUntil": "19 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/35/751",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/f1b316bb2885bc60811b3ca91c4303dc.jpg",
   "location": [
    "Ruko Pondok Indah Tiban Blok C No.02 Batam"
   ]
  },
  {
   "title": "Installment 0% di Toko Andalas Bangunan",
   "logoImage": "http://m.bnizona.com/files/c0ae0230b732b1dbdc141d3341bc00af.jpg",
   "merchantName": "Toko Andalas Bangunan",
   "validUntil": "14 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/35/749",
   "content": "<ul><br>\n\t<li>Installment 0% periode 6 dan 12&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/00fcf9106142177b7b52b28a6eed401c.jpg",
   "location": [
    "Jl. Andalas Raya No.86 Padang"
   ]
  },
  {
   "title": "Penawaran spesial di RY Auto Services",
   "logoImage": "http://m.bnizona.com/files/d966aa132fdcc2f2d1a82524f8c7c951.jpg",
   "merchantName": "RY Auto Services",
   "validUntil": "23 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/35/716",
   "content": "<strong>Installment 0% (24 Maret 2015 - 23 Maret 2016)</strong><br>\n<ul><br>\n\t<li>Installment 0% periode 3 dan 6&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n<br><br>\n<strong>Diskon 15% (24 Maret 2015 - 23 Juni 2015)</strong><br>\n<br>\n<ul><br>\n\t<li>Diskon 15% utk Injector Cleaner + Service</li><br>\n\t<li>Pembelian Velg tertentu free Spooring and Balancing</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah)</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/4d23108e5b46edf8f19d2e057fcdac66.jpg",
   "location": [
    "Jl. HR Soebrantas Pekanbaru"
   ]
  },
  {
   "title": "Penawaran spesial di MZR Auto Services",
   "logoImage": "http://m.bnizona.com/files/cfb24a180de56c7496c26771a1fc5ae8.jpg",
   "merchantName": "MZR Auto Services",
   "validUntil": "23 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/35/715",
   "content": "<strong>Installment 0% (24 Maret 2015 - 23 Maret 2016)</strong><br>\n<ul><br>\n\t<li>Installment 0% periode 3 dan 6&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n<br><br>\n<strong>Diskon 15% (24 Maret 2015 - 23 Juni 2015)</strong><br>\n<br>\n<ul><br>\n\t<li>Diskon 15% utk Injector Cleaner + Service</li><br>\n\t<li>Pembelian Velg tertentu free Spooring and Balancing</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah)</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/27a37bf2cf8c329540b80b13fba7a017.jpg",
   "location": [
    "Jl. Aridfin Ahmad Pekanbaru",
    "Jl. Harapan raya Pekanbaru"
   ]
  },
  {
   "title": "Penawaran spesial di EFO Auto Services",
   "logoImage": "http://m.bnizona.com/files/e003a5e2668f1424689f06f54c494ddd.jpg",
   "merchantName": "EFO Auto Services",
   "validUntil": "23 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/35/714",
   "content": "<strong>Installment 0% (24 Maret 2015 - 23 Maret 2016)</strong><br>\n<ul><br>\n\t<li>Installment 0% periode 3 dan 6&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n<br><br>\n<strong>Diskon 15% (24 Maret 2015 - 23 Juni 2015)</strong><br>\n<br>\n<ul><br>\n\t<li>Diskon 15% utk Injector Cleaner + Service</li><br>\n\t<li>Pembelian Velg tertentu free Spooring and Balancing</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah)</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/945ebb06adf943bf3e14ae82e97dd096.jpg",
   "location": [
    "Jl. T Tambusai Pekanbaru",
    "Jl. Kavling/Samarinda Pekanbaru"
   ]
  },
  {
   "title": "Penawaran spesial di BBC Oto Service",
   "logoImage": "http://m.bnizona.com/files/d2f0bbe6cd36a80547eaa6175bb284b1.jpg",
   "merchantName": "BBC Oto Service",
   "validUntil": "23 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/35/713",
   "content": "<strong>Installment 0% (24 Maret 2015 - 23 Maret 2016)</strong><br>\n<ul><br>\n\t<li>Installment 0% periode 3 dan 6&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI (kecuali Hasanah Card dan Corporate Card)</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n<strong>Diskon 15% (24 Maret 2015 - 23 Juni 2015)</strong><br>\n<br>\n<ul><br>\n\t<li>Diskon 15% utk Injector Cleaner + Service</li><br>\n\t<li>Pembelian Velg tertentu free Spooring and Balancing</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah)</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n\t<li>Berlaku setiap hari selama periode program</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/41e73a935f5a3bf0f56a3c7b5533db81.jpg",
   "location": [
    "Jl. Imam Munandar Pekanbaru"
   ]
  },
  {
   "title": "Penawaran spesial di ARD Auto Service",
   "logoImage": "http://m.bnizona.com/files/7f4e9f863ab8caab4e50ceaa233ca7a0.jpg",
   "merchantName": "ARD Auto Service",
   "validUntil": "23 March 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/35/712",
   "content": "<strong>Installment 0% (24 Maret 2015 - 23 Maret 2016)</strong><br>\n<ul><br>\n\t<li>Installment 0% periode 3 dan 6&#xA0; bulan dengan Kartu Kredit BNI selama periode program</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah).</li><br>\n\t<li>Berlaku untuk semua produk</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n</ul><br>\n<br><br>\n<strong>Diskon 15% (24 Maret 2015 - 23 Juni 2015)</strong><br>\n<br>\n<ul><br>\n\t<li>Diskon 15% utk Injector Cleaner + Service</li><br>\n\t<li>Pembelian Velg tertentu free Spooring and Balancing</li><br>\n\t<li>Minimal Transaksi Rp 500.000, (lima ratus&#xA0; ribu rupiah)</li><br>\n\t<li>Berlaku semua jenis Kartu Kredit BNI ( kecuali Hasanah Card dan Corporate Card )</li><br>\n\t<li>Berlaku setiap hari termasuk tanggal merah dan weekend</li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/8c6675ce7e31acd3dd39067a94a87d37.jpg",
   "location": [
    "Jl. HR Soebrantas Pekanbaru"
   ]
  }
 ],
 "Hobbies": [
  {
   "title": "BNI JAVA JAZZ ON THE MOVE",
   "logoImage": "http://m.bnizona.com/files/f845be280c76043aba10fd47be51d975.jpeg",
   "merchantName": "BNI ",
   "validUntil": "26 February 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/39/1171",
   "content": "Khusus Pegawai BNI Se-Jabodetabek!<br><br>\n<strong>#BNIJavaJazz2016 LIVE TWEET COMPETITION</strong><br><br>\nNikmati musiknya, capture momentnya &amp; share ke akun twitter atau instagram-mu. Menangkan <em><strong>daily ticket </strong></em>Java Jazz Festival 2016!<br><br>\nPeriode upload foto : 26 Februari 2016 pukul 08.00 &#x2013; 22.00 WIB<br><br>\n<strong>Syarat &amp; Ketentuan :</strong><br><br>\n1. Upload foto-foto seputar Java Jazz On The Move (JJOTM) di area Lobby Kantor Pusat BNI pada tanggal 26 Februari 2016 di akun twitter atau instagram-mu.<br><br>\n2. Wajib mencantumkan hashtag&#xA0; <strong>#BNIJavaJazz2016</strong> pada caption foto.<br><br>\n3. Wajib mention akun <strong>twitter / instagram @BNI46</strong>.<br><br>\n4. Kontes berlaku untuk pegawai BNI se-Jabodetabek.<br><br>\n5. Peserta wajib follow akun twitter / instagram @BNI46.<br><br>\n6. Tiap peserta dapat mem-posting foto yang berbeda sebanyak-banyaknya.<br><br>\n7. Jenis foto yang disarankan adalah foto yang memperlihatkan kemeriahan acara JJOTM di area Lobby Kantor Pusat BNI tanggal 26 Februari 2016.<br><br>\n8. Foto atau komentar yang dikirimkan tidak diperkenankan mengandung unsur provokatif, pornografi dan SARA.<br><br>\n<strong>Hadiah :</strong><br><br>\n1. 5 (lima) foto terbaik masing-masing mendapatkan 1 (satu) daily ticket BNI Java Jazz Festival 2016.<br><br>\n2. Pengumuman pemenang pada tanggal 1 Maret 2016 melalui email newsletter dan BNI Forum.<br><br>\n3. Keputusan juri tidak dapat diganggu gugat dan bersifat final.<br><br>\n4. Pemenang akan dikonfirmasi via DM (<em>Dirrect Message</em>).<br><br>\n5. Hadiah diambil langsung ke Divisi CMM - Wisma BNI 46 lantai 43 Jl. Jendral Sudirman Kav. 1 pada tanggal 2-4 Maret 2015 pukul 10.00-17.00 WIB (Cp. Putri/2511946 ext. 2575) dengan membawa fotocopy &amp; asli TPP dan Identitas Diri.",
   "image": "http://m.bnizona.com//files/938e0399b4fea5f82149ddc3b27f8fb6.jpg",
   "phone": "021500046",
   "location": [
    "Gedung BNI",
    "Jl. Jend. Sudirman Kav.1, Jakarta, 10220"
   ]
  },
  {
   "title": "Mari Belajar di 88 ENGLISH Surabaya, Diskon 30%",
   "logoImage": "http://m.bnizona.com/files/cc8b1130e7c88c6ef0466fe992c91945.jpg",
   "merchantName": "88 ENGLISH",
   "validUntil": "08 May 2016",
   "href": "http://m.bnizona.com/index.php/promo/view/39/846",
   "content": "<ul><br>\n\t<li>Diskon 30% untuk program General English dengan semua Kartu Kredit BNI di 88 English Surabaya.</li><br>\n\t<li>Berlaku cicilan 0% (6 dan 12 bulan) minimal transaksi Rp. 500.000,- selama periode program.</li><br>\n\t<li>Promo tidak dapat digabungkan dengan promo lainnya yang sedang berlangsung.</li><br>\n\t<li>Berlaku untuk semua jenis Kartu Kredit BNI tidak termasuk Corporate Card &amp; Hasanah Card.</li><br>\n\t<li>Berlaku hingga <strong>8 Mei 2016</strong></li><br>\n</ul><br>\n",
   "image": "http://m.bnizona.com//files/38f9b32fe54d8d0a669bf1aadd633a8f.jpg",
   "phone": " 031 5034383",
   "location": [
    "Graha SA lt. 3, Jl. Raya Gubeng 1921 Surabaya"
   ]
  }
 ]
}
```