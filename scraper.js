var Promise = require('bluebird');
var request = require('request');
var cheerio = require('cheerio');
var config = require('./config');

var scrapCategories = function() {
    return new Promise(function(resolve, reject) {
        request(config.url.category, function(err, res, body) {
            if (!err && res.statusCode === 200) {
                var data = [];
                var $ = cheerio.load(body);

                $('.menu li a').each(function() {
                    data.push({
                        text: $(this).text(),
                        href: $(this).attr('href'),
                    });
                });
                resolve(data);
            } else {
                reject(res);
            }
        });
    });
};

var getItems = function(params) {
    return new Promise(function(resolve, reject) {
        // explicit for readability
        var req = {
            categoryId: params.categoryId,
            pageStart: (params.pageStart) ? params.pageStart : 0
        };

        var url = config.url.items + '/' + req.categoryId + '/' + req.pageStart;
        request(url, function(err, res, body) {
            if (!err && res.statusCode === 200) {
                var data = [];
                var $ = cheerio.load(body);

                if ($('li').length > 0) {
                    $('li').each(function() {
                        var $self = $(this);
                        var $link = $self.children('a');
                        data.push({
                            title: $link.children('.promo-title').text(),
                            logoImage: $link.children('img').attr('src'),
                            merchantName: $link.children('.merchant-name').text(),
                            validUntil: $link.children('.valid-until').text().replace(/valid until /g, ''),
                            href: $link.attr('href')
                        });
                    });

                    getItems({categoryId: req.categoryId, pageStart: req.pageStart + 10}).then(function(dat) {
                        dat.forEach(function(d) {
                            data.push(d);
                        });
                    }).then(function() {
                        resolve(data);
                    });
                } else {
                    resolve(data);
                }
            } else {
                reject(res);
            }
        });
    });
};

var scrapItems = function(params) {
    return new Promise(function(resolve, reject) {
        // explicit for readability
        var req = {
            label: params.label,
            parentUrl: params.parentUrl,
            pageStart: (params.pageStart) ? params.pageStart : 0
        };

        var tokenizedUrl = req.parentUrl.split('/');
        var categoryId = tokenizedUrl[tokenizedUrl.length - 1];

        getItems({categoryId: categoryId, pageStart: req.pageStart}).then(function(data) {
            var res = {};
            res[req.label] = data;
            resolve(res);
        }, function(err) {reject(err)});
    });
};

var scrapContent = function(params) {
    return new Promise(function(resolve, reject) {
        var req = {
            url: params.url
        };

        request(req.url, function(err, res, body) {
            if (!err && res.statusCode === 200) {
                var data = {};
                var $ = cheerio.load(body);

                var $merchantLocation = $('.content.merchant');
                data.image = $('.banner img').attr('src');
                data.content = $('#merchant-detail.content p').html();
                data.phone = $merchantLocation.children('p').last().text().replace(/[-]/g, '');
                if (data.phone === '') delete(data.phone);

                data.location = [];

                var location = $merchantLocation.children('p').first().html().split(/<br>|\n/g);
                for (var i in location) {
                    location[i] = location[i].replace(/[-]|<[^>]*>|&#xA0;/g, '').trim();
                    if (location[i] !== '') {
                        data.location.push(location[i]);
                    }
                }
                resolve(data);
            } else {
                reject(err);
            }
        });
    });
};


var scrapAllCategoryItems = function() {
    return new Promise(function(resolve, reject) {
        scrapCategories().then(function(data) {
            var promises = [];
            for (var i in data) {
                promises.push(scrapItems({parentUrl: data[i].href, label: data[i].text}));
            }

            var res = {};
            Promise.all(promises).then(function(dat) {
                for (var i in dat) {
                    for (var k in dat[i]) {
                        res[k] = dat[i][k]; 
                    }
                }
                resolve(res);
            }, function(err) {reject(err)});
            return res;
        });
    });
};

var scrapAllData = function() {
    return new Promise(function(resolve, reject) {
        scrapAllCategoryItems().then(function(data) {
            var promises = [];
            for (var category in data) {
                for (var i in data[category]) {
                    promises.push(scrapContent({url: data[category][i].href}));
                }
            }

            var res = data;
            Promise.all(promises).then(function(dat) {
                var idx = 0;
                for (var category in res) {
                    for (var i in res[category]) {
                        res[category][i].content = dat[idx].content;
                        res[category][i].image = dat[idx].image;
                        res[category][i].phone = dat[idx].phone;
                        res[category][i].location = dat[idx].location;
                        ++idx;
                    }
                }
                resolve(res);
            }, function(err) {reject(err)});
            return res;
        })
    });
}

module.exports = {
    scrapAllData: scrapAllData,
    scrapAllCategoryItems: scrapAllCategoryItems,
    scrapCategories: scrapCategories,
    scrapContent: scrapContent
};