var http = require('http');
var util = require('./util');
var scraper = require('./scraper');


var server = http.createServer(function(req, res) {
    if (req.url === '/items') {
        scraper.scrapAllCategoryItems().then(function(data) {
            util.createResponse(200, data, res, 1);
        });
    } else {
        scraper.scrapAllData().then(function(data) {
            util.createResponse(200, data, res, 1);
        });
    }
});

server.listen(5555);